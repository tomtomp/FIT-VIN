/**
 * @file TextLog.h
 * @author Tomas Polasek
 * @brief Text message logging.
 */

#ifndef QUANTIZER_TEXTLOG_H
#define QUANTIZER_TEXTLOG_H

#include "Types.h"

#include <gtkmm/textview.h>

/// Namespace for utility functions and classes.
namespace util
{
    /// Message types.
    enum class MsgType
    {
        info,
        warning,
        error,
        critical
    };

    /// Wrapper around GtkTextBuffer used for logging messages.
    class TextLog
    {
    public:
        /// Initialize the TextLog.
        TextLog() = default;

        TextLog(const TextLog &o) = delete;

        TextLog(TextLog &&o) = delete;

        TextLog &operator=(const TextLog &r) = delete;

        TextLog &operator=(TextLog &&r) = delete;

        /// Use given text view owned by outside widget.
        void setTextView(Gtk::TextView *textView);

        /**
         * Log given message and update inner text buffer, if present.
         * @tparam Appendable Type which can be appended to a string.
         * @param type Type of message.
         * @param msg Message itself.
         */
        template<typename Appendable>
        void logMessage(MsgType type, const Appendable &msg);

    private:
        /// Get message prefix string for given message type.
        const char *getMsgPrefix(MsgType type);

        /// Text view representing the log.
        Gtk::TextView *mTextView{nullptr};
    protected:
    };

    // Template implementation.
    template<typename Appendable>
    void TextLog::logMessage(MsgType type, const Appendable &msg)
    {
        Glib::ustring message;
        message.append(getMsgPrefix(type));
        message.append(msg);

        // Source: https://mail.gnome.org/archives/gtk-list/2007-May/msg00034.html
        auto tb = mTextView->get_buffer();

        auto mark = tb->get_insert();
        auto iter = tb->end();

        tb->move_mark(mark, iter);
        tb->insert_at_cursor(message);
        mTextView->scroll_to(mark, 0.0, 0.5, 1.0);
    }
}

#endif //QUANTIZER_TEXTLOG_H
