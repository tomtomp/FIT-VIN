/**
 * @file Types.h
 * @author Tomas Polasek
 * @brief Type defitions for this application.
 */

#ifndef QUANTIZER_TYPES_H
#define QUANTIZER_TYPES_H

// Standard library includes
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <sstream>
#include <unordered_map>
#include <random>

#endif //QUANTIZER_TYPES_H
