/**
 * @file GLSLSrcSplatter.h
 * @author Tomas Polasek
 * @brief GLSL source code pre-processor.
 */

#ifndef QUANTIZER_GLSLSRCSPLATTER_H
#define QUANTIZER_GLSLSRCSPLATTER_H

#include "Types.h"

/// Namespace for utility functions and classes.
namespace util
{
    /// Pre-processor mark types.
    namespace ppMarks
    {
        /// Mark used for '#' pre-processor directives.
        static constexpr char const *DIRECTIVE{"#DIRECTIVE#"};
        /// Mark used for coordinate pre-processor directives.
        static constexpr char const *COORD_DIRECTIVE{"#COORD_DIRECTIVE#"};
        /// Mark used for input pre-processor directives.
        static constexpr char const *INPUT_DIRECTIVE{"#INPUT_DIRECTIVE#"};
        /// Mark used for user source code.
        static constexpr char const *SRC{"#SRC#"};
        /// Mark used for orbit source code.
        static constexpr char const *ORB{"#ORB#"};
    }

    /// GLSL source code pre-processor.
    class GLSLSrcSplatter
    {
    public:
        /// Default constructor with empty template.
        GLSLSrcSplatter() : GLSLSrcSplatter("") {}

        /**
         * Load GLSL source template from given file.
         * @param tFilename Template filename.
         */
        GLSLSrcSplatter(const std::string &tFilename)
        { populateMarkMap(); loadSrcTemplate(tFilename); }

        /**
         * Load GLSL source code directly from given character array.
         * @param tSrc Template source code.
         */
        GLSLSrcSplatter(const char *tSrc)
        { populateMarkMap(); loadSrcTemplate(tSrc); }

        /**
         * Load GLSL source template from given file.
         * @param tFilename Template filename.
         */
        void loadSrcTemplate(const std::string &tFilename);

        /**
         * Load GLSL source code directly from given character array.
         * @param tSrc Template source code.
         */
        void loadSrcTemplate(const char *tSrc);

        /**
         * Set value for given pre-processor mark.
         * @param mark Mark identifier, e.g. ppMarks::SRC.
         * @param value Value substituted for the given mark.
         */
        void setMarkValue(std::string mark, const std::string &value);

        /**
         * Append given value to the already existing mark value.
         * @param mark Mark identifier, e.g. ppMarks::SRC.
         * @param value Value substituted for the given mark.
         */
        void appendMarkValue(std::string mark, const std::string &value);

        /// Getter for the result source code.
        const std::string &src()
        { recalculateSrc(); return mSrc; }
        /// Getter for the result source code.
        const char *c_src()
        { recalculateSrc(); return mSrc.c_str(); }
    private:
        /// Create basic structure in the mark map.
        void populateMarkMap();
        /// Recalculate the result source code.
        void recalculateSrc();

        /**
         * Replace all occurrences in given string with given substitute.
         * @param src String in which the substitution should be performed in.
         * @param from Which string should be replaced.
         * @param to Substitute to the replaced string.
         */
        void replaceAll(std::string &src, const std::string &from, const std::string &to);

        /// Pure template, without substitution.
        std::string mTemplate;
        /// Mapping from mark types to mark values.
        std::unordered_map<std::string, std::string> mMarkValues;
        /// Dirty flag for recalculating the source code.
        bool mDirty;
        /// Source code with substituted marks.
        std::string mSrc;
    protected:
    }; // class GLSLSrcSplatter
} // namespace util

#endif //QUANTIZER_GLSLSRCSPLATTER_H
