/**
 * @file WorkArea.h
 * @author Tomas Polasek
 * @brief Work area in the middle of the main window.
 */

#ifndef QUANTIZER_WORKAREA_H
#define QUANTIZER_WORKAREA_H

#include "Types.h"
#include "Plane.h"
#include "Projection.h"
#include "GLSLProgram.h"
#include "GLSLSrcSplatter.h"
#include "TextLog.h"
#include "ColorChooser.h"
#include "ColorSampler.h"
#include "HelpWindow.h"

// Gtkmm includes
#include <gtkmm/hvbox.h>
#include <gtkmm/glarea.h>
#include <gtkmm/builder.h>
#include <gtkmm/textview.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/revealer.h>
#include <gtkmm/switch.h>
#include <gtkmm/scale.h>
#include <gtkmm/grid.h>
#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>
#include <gtkmm/main.h>
#include <glibmm/main.h>

#include "GlTypes.h"

// ImageMagick C++ library.
#include <Magick++.h>

/// Namespace for utility functions and classes.
namespace util
{
    /// Get extension from given filename.
    std::string getExtension(const std::string &fileName);
}

/// Namespace containing my widget types.
namespace mw
{
    /**
     * Work area in the middle of the main window.
     */
    class WorkArea : public Gtk::VBox
    {
    public:
        /// Constructor.
        static Glib::RefPtr<WorkArea> create();

        /// Redraw the draw area.
        void redraw();

        /**
         * Get the current configuration of the work area as a string.
         * @return String representing the configuration.
         */
        std::string getConfig() const;

        /**
         * Load configuration from string.
         * @param cfg Configuration represnted as a string.
         */
        void loadConfig(const std::string &cfg);

        /**
         * Export image using current settings.
         * @param fileName File to save the result to.
         */
        void exportImage(const std::string &fileName);

        /**
         * Export animation using current settings.
         * @param fileName File to save the result to.
         */
        void exportAnimation(const std::string &fileName);

        /// Calculate number of frames in animation for current settings.
        std::size_t exportAnimationFrames();

        /// Calculate how long should each frame stay displayed.
        std::size_t exportAnimationFrameTime();
    private:
        /// Howl ong must there be no changes of the input function in order to compile.
        static constexpr uint32_t WAIT_BEFORE_COMPILE{200};
        /// Starting top point.
        static constexpr float START_TOP{1.0f};
        /// Starting bottom point.
        static constexpr float START_BOTTOM{-1.0f};
        /// Starting left point.
        static constexpr float START_LEFT{-1.0f};
        /// Starting right point.
        static constexpr float START_RIGHT{1.0f};
        /// Starting scale.
        static constexpr float START_SCALE{1.0f};
        /// Default start function mapping interval.
        static constexpr float START_FUN{-1.0f};
        /// Default end function mapping interval.
        static constexpr float END_FUN{1.0f};
        /// Default x cursor coordinate.
        static constexpr float CURSOR_POS_X{0.0f};
        /// Default y cursor coordinate.
        static constexpr float CURSOR_POS_Y{0.0f};
        /// Default additional value for polar.
        static constexpr float POLAR_ADD_X{1.0f};
        /// Default additional value for polar.
        static constexpr float POLAR_ADD_Y{1.0f};
        /// Default additional value for circular.
        static constexpr float CIRCULAR_ADD_X{1.0f};
        /// Default additional value for circular.
        static constexpr float CIRCULAR_ADD_Y{1.0f};
        /// Default start time.
        static constexpr float TEMPORAL_START{0.0f};
        /// Default end time.
        static constexpr float TEMPORAL_END{1.0f};
        /// Default period in seconds.
        static constexpr float TEMPORAL_PERIOD{1.0f};
        /// Default frames per second for animation.
        static constexpr float TEMPORAL_FPS{60.0f};
        /// Number of milliseconds in one second.
        static constexpr float MS_IN_S{1000.0f};
        /// Default value for the supersampling input.
        static constexpr uint32_t DEFAULT_SUPERSAMPLING{1};
        /// Default extension for image files.
        static constexpr char const *DEFAULT_IMAGE_EXTENSION{"jpg"};
        /// Default extension for animation files.
        static constexpr char const *DEFAULT_ANIMATION_EXTENSION{"gif"};

        /// Empty GLSL pre-processor directive.
        static constexpr char const *EMPTY_DIRECTIVE{""};
        /// Pre-processor directive specifying that the input is simple style.
        static constexpr char const *INPUT_SIMPLE{"#define SIMPLE"};
        /// Pre-processor directive specifying that the input is orbit style.
        static constexpr char const *INPUT_ORBIT{"#define ORBIT"};
        /// Pre-processor directive specifying that the input is advanced style.
        static constexpr char const *INPUT_ADVANCED{"#define ADVANCED"};

        /// Filter types which are selectable in GUI.
        enum class FilterType
        {
            LINEAR,
            NEAREST,
            MAX
        };

        /// Coordinate systems which are selectable in GUI.
        enum class CoordType
        {
            CARTESIAN,
            POLAR,
            CIRCULAR,
            MAX
        };
        /// GLSL pre-processor directive for cartesian coordinate system.
        static constexpr char const *COORD_CARTESIAN{"#define CARTESIAN"};
        /// GLSL pre-processor directive for polar coordinate system.
        static constexpr char const *COORD_POLAR{"#define POLAR"};
        /// GLSL pre-processor directive for circular coordinate system.
        static constexpr char const *COORD_CIRCULAR{"#define CIRCULAR"};

        /// Time variable styles which are selectable in GUI.
        enum class TemporalType
        {
            LINEAR,
            PERIODIC,
            MAX
        };
        /// Default temporal type.
        static constexpr TemporalType TEMPORAL_TYPE{TemporalType::LINEAR};

        /// Function types which are selectable in GUI.
        enum FunctionType
        {
            SIMPLE,
            ORBIT,
            ADVANCED,
            MAX
        };
        // Default function type.
        static constexpr FunctionType FUNCTION_TYPE{FunctionType::SIMPLE};

        /// Helper class for representing the resolution combo-box model.
        struct ResolutionModel : public Gtk::TreeModel::ColumnRecord
        {
            ResolutionModel()
            { add(mColumnId); add(mColumnValue); }
            Gtk::TreeModelColumn<int> mColumnId;
            Gtk::TreeModelColumn<Glib::ustring> mColumnValue;
        }; // struct ResolutionModel

        /// Resolution value wrapper.
        struct ResolutionValue
        {
            constexpr ResolutionValue(uint32_t w, uint32_t h) :
                    width{w}, height{h} { }

            // Simple string constructor.
            std::string str() const
            { return std::to_string(width) + "x" + std::to_string(height); }

            uint32_t width;
            uint32_t height;
        }; // struct ResolutionValue

        /// Structure used for push/pop of the animation state.
        class AnimationSaveState
        {
        public:
            /// Remember animation data from given WorkArea.
            AnimationSaveState(WorkArea *workArea);
            /// Restore the data to WorkArea provided in the constructor.
            ~AnimationSaveState();
        private:
            /// WorkArea to restore.
            WorkArea *mWorkArea;
            /// Animation start time.
            float mAnimStart;
            /// Animation end time.
            float mAnimEnd;
            /// Current animation time.
            float mAnimState;
            /// By how much time should the animation advance.
            float mAnimAdvance;
        protected:
        }; // struct AnimationSaveState

        /// Structure used for push/pop of viewport state.
        class ViewportSaveState
        {
        public:
            /// Remember viewport state.
            ViewportSaveState(WorkArea *workArea);
            /// Restore the viewport state.
            ~ViewportSaveState();
        private:
            /// WorkArea to restore.
            WorkArea *mWorkArea;
            /// Original width of the viewport.
            int mWidth;
            /// Original height of the viewport.
            int mHeight;
        protected:
        }; // struct AnimationSaveState

        /// List of supported resolutions.
        static const ResolutionValue RESOLUTIONS[];

        /// OpenGL initialization.
        void glInit();
        /// OpenGL inner initialization.
        void glInitInner();
        /// Get locations for all of the uniforms.
        void getUniformLocations();
        /// OpenGL de-initialization.
        void glDestroy();
        /// Called when OpenGL should draw a frame.
        bool glRender(const Glib::RefPtr<Gdk::GLContext> &context);
        /// Called when OpenGL viewport is resized.
        void glResize(int width, int height);
        /**
         * Button press handler for the draw area.
         * @param event Event triggered.
         * @return Returns true, if the other hadlers should not get it.
         */
        bool buttonPress(GdkEventButton *event);
        /**
         * Button release handler for the draw area.
         * @param event Event triggered.
         * @return Returns true, if the other hadlers should not get it.
         */
        bool buttonRelease(GdkEventButton *event);
        /**
         * Mouse movement handler for the draw area.
         * @param event Event triggered.
         * @return Returns true, if the other hadlers should not get it.
         */
        bool mouseMove(GdkEventMotion *event);
        /**
         * Mouse scroll handler for the draw area.
         * @param event Event triggered.
         * @return Returns true, if th other handlers should not get it.
         */
        bool mouseScroll(GdkEventScroll *event);

        /**
         * Callback, when input function is changed.
         */
        void inputChange();
        /**
         * Timeout callback, when input should be checked/compiled.
         * @return Return false to stop disconnect the timeout.
         */
        bool checkInput();

        /// Parse input and set the required directives.
        void processInput(util::GLSLSrcSplatter &src, Glib::ustring &text);

        /// Callback when new colors are selected.
        void colorsChanged(bool);

        /// Callback when user select new filter type.
        void filterTypeChange(FilterType newType);
        /// Callback when user select new coordinate system type.
        void coordTypeChange(CoordType newType);
        /// Callback when user select new temporal variable type.
        void temporalTypeChange(TemporalType newType);
        /// Callback when user select new function type.
        void functionTypeChange(FunctionType newType);

        /// Callback when one of the coordinate window dimensions change.
        void coordWindowChange();

        /// Callback when RNG seed is changed in GUI.
        void seedChange();
        /// Generate new random RNG seed.
        void generateSeed();

        /// Callback when mapping interval for color changes.
        void colorMapChange();

        /// Recalculate function transform vector.
        void recalcFunTransform();

        /// Callback when cursor position changes.
        void cursorChange();

        /// Callback when additional data for polar change.
        void polarAddChange();
        /// Callback when additional data for circular change.
        void circularAddChange();

        /// Use the polar additional data.
        void setPolarAdd();
        /// Use the circular additional data.
        void setCircularAdd();

        /// Callback when animation is enabled/disabled.
        void temporalSwitch();
        /// Callback when one of the ends of the time interval changed.
        void timeSpanChanged();
        /// Callback when time period changes.
        void timePeriodChanged();
        /// Callback when animation fps changes.
        void timeFpsChanged();

        /// Reset animation parameters and begin animating.
        void setupAnimation(bool ui = true);
        /// Stop the animation.
        void disableAnimation();
        /// Move to the next step of the animation, does not redraw.
        void advanceAnimation(bool setTimeline = true);

        /// Callback from the animation signal.
        bool advanceAnimationRedraw();

        /// Callback when the timeline is used.
        bool timeLineUsed(Gtk::ScrollType scroll, double newValue);
        /// Callback when value of the timeline changes.
        void timeLineChanged();

        /// Display the help dialog.
        void displayHelp();

        /// Get the currently selected resolution.
        ResolutionValue getCurrentResolution();

        /**
         * Fill given variables with OpenGL object handles.
         * @param srb Supersample render buffer.
         * @param sfbo Supersample framebuffer.
         * @param rt Resolve texture.
         * @param rfbo Resolve framebuffer.
         * @param res Requested resolution.
         * @param supersample Requested supersample multiplier.
         */
        void prepareExportStructures(GLuint &srb, GLuint &sfbo, GLuint &rt, GLuint &rfbo,
                                     ResolutionValue &res, GLsizei supersample);

        /// Destroy structures created by the prepareExportStructure function.
        void destroyExportStructures(GLuint &srb, GLuint &sfbo, GLuint &rt, GLuint &rfbo);

        /**
         * Prepare buffer for the image data.
         * @param res Resolution of the image.
         * @return Returns vector with required size.
         */
        std::vector<unsigned char> prepareDataBuffer(ResolutionValue &res);

        /**
         * Resolve given framebuffers and load data from them.
         * @param sfbo Super-sampled framebuffer.
         * @param rt Resolve texture.
         * @param rfbo Resolve framebuffer.
         * @param res Resolution of the resolve image.
         * @param supersample Requested supersample multiplier.
         * @param data Data buffer for the result image.
         * @return Returns vector containing the image data.
         */
        std::vector<unsigned char> resolveFramebuffer(GLuint sfbo, GLuint rt, GLuint rfbo,
                                                      ResolutionValue &res, GLsizei supersample,
                                                      std::vector<unsigned char> &data);
    protected:
        /// Allow access to the protected constructor.
        friend class Gtk::Builder;

        /**
         * Construct work area loaded using the gtk builder.
         * @param object Loaded object.
         * @param builder Builder used to load it.
         */
        WorkArea(BaseObjectType *object,
                 const Glib::RefPtr<Gtk::Builder> &builder);
        virtual ~WorkArea();

        /// OpenGL drawing area.
        Gtk::GLArea *mDrawArea;
        /// Function specification input.
        Gtk::TextView *mInput;
        /// Message log.
        Gtk::TextView *mLog;
        /// Logging utility.
        util::TextLog mTextLog;
        /// Connection used for checking/compiling the function input.
        sigc::connection mCheckConn;
        /// Scale used for changing current time.
        Gtk::Scale *mTimeLine;
        /// Inner value of the timeline.
        //Gtk::Adjustment *mTemporalTimeLine;
        /// Connection used for scheduling the animation.
        sigc::connection mAnimConn;
        /// Window containing help about the function intpu.
        Glib::RefPtr<mw::HelpWindow> mHelpWindow;
        /// Button which can be used to display the help window.
        Gtk::Button *mFunctionHelp;

        // Color:
        Gtk::Box *mColorBox;
        Gtk::Scale *mColorStart;
        Gtk::Scale *mColorEnd;
        Gtk::RadioButton *mColorLinear;
        Gtk::RadioButton *mColorNearest;
        mw::ColorChooser *mColorChooser;

        // Coordinate space:
        Gtk::RadioButton *mCoordCartesian;
        Gtk::RadioButton *mCoordPolar;
        Gtk::RadioButton *mCoordCircular;
        Gtk::Revealer *mCoordPolarAdd;
        Gtk::SpinButton *mCoordPolarAddX;
        Gtk::SpinButton *mCoordPolarAddY;
        Gtk::Revealer *mCoordCircularAdd;
        Gtk::SpinButton *mCoordCircularAddX;
        Gtk::SpinButton *mCoordCircularAddY;
        Gtk::SpinButton *mCoordLeft;
        Gtk::SpinButton *mCoordTop;
        Gtk::SpinButton *mCoordRight;
        Gtk::SpinButton *mCoordBottom;
        Gtk::SpinButton *mCoordCursorX;
        Gtk::SpinButton *mCoordCursorY;

        // Random:
        Gtk::SpinButton *mRandomSeed;
        Gtk::Button *mRandomGenerate;

        // Temporal:
        Gtk::Switch *mTemporalAnimate;
        Gtk::SpinButton *mTemporalStart;
        Gtk::SpinButton *mTemporalEnd;
        Gtk::SpinButton *mTemporalCurrent;
        Gtk::RadioButton *mTemporalLinear;
        Gtk::RadioButton *mTemporalPeriodic;
        Gtk::SpinButton *mTemporalPeriod;
        Gtk::SpinButton *mTemporalFps;

        // Function:
        Gtk::RadioButton *mFunctionSimple;
        Gtk::RadioButton *mFunctionOrbit;
        Gtk::RadioButton *mFunctionAdvanced;

        // Export:
        Gtk::ComboBox *mExportResolution;
        Gtk::SpinButton *mExportSupersampling;

        ResolutionModel mExportResolutionColumns;
        Glib::RefPtr<Gtk::ListStore> mExportResolutionModel;

        /// Data used in the application.
        struct WorkData
        {
            /// Source pre-processor for the fragment shader.
            util::GLSLSrcSplatter glFragSrc;
            /// Shader program.
            util::GLSLProgram glProgram;
            /// Model-View-Projection matrix.
            util::Projection glMvp;
            /// Multiplicative and additive part used for transforming function output.
            glm::vec2 glFunT;
            /// Position of the cursor, which can be used in the fuctions.
            glm::vec2 glCursorPos;
            /// Additional data passable to the shader.
            glm::vec2 glAdditional;
            /// Plane used as drawing surface.
            util::Plane glPlane;
            /// Color sampler from the color selector.
            util::ColorSampler glSampler;

            /// Uniform location of the MVP matrix.
            GLint glUPosMvp;
            /// Uniform location of the function transform vector.
            GLint glUFunT;
            /// Uniform location of the cursor position.
            GLint glUCursorPos;
            /// Uniform location of the additional data.
            GLint glUAdditional;
            /// Uniform location of the color sampler.
            GLint glUColorS;
            /// Uniform location of the time variable.
            GLint glUTime;
            /// Uniform location of the RNG seed.
            GLint glUSeed;

            /// Time send to the shader.
            float glTime;
            /// Seed send to the shader.
            float glSeed;

            /// RNG engine.
            std::default_random_engine re;

            /// Flag to skip redrawing the draw area.
            bool skipRedraw;
            /**
             * If redraw event is triggered, this flag
             * specifies if redraw should actually occur.
             */
            bool skipDraw;

            /// Current animation style.
            TemporalType animStyle;
            /// Animation start time.
            float animStart;
            /// Animation end time.
            float animEnd;
            /// Current animation time.
            float animState;
            /// By how much time should the animation advance.
            float animAdvance;

            /// Current function type.
            FunctionType functionType;

            /// Flag for dragging with mouse.
            bool mouseDrag;
            /// Original mouse cursor value before dragging.
            glm::vec2 mouseDragOrig;
            /// Where the drag started at.
            glm::vec2 mouseDragStart;
        } mData;
    }; // class WorkArea

    /// Namespace containing config file tag names.
    namespace cfg
    {
        static constexpr char const *CFG_GROUP{"Input"};

        static constexpr char const *CFG_INPUT{"Input"};

        static constexpr char const *CFG_COLOR_START{"ColorStart"};
        static constexpr char const *CFG_COLOR_END{"ColorEnd"};
        static constexpr char const *CFG_COLOR_STYLE{"ColorStyle"};

        static constexpr char const *CFG_COORD_STYLE{"CoordStyle"};
        static constexpr char const *CFG_COORD_POLAR_ADD_X{"CoordPolarAddX"};
        static constexpr char const *CFG_COORD_POLAR_ADD_Y{"CoordPolarAddY"};
        static constexpr char const *CFG_COORD_CIRCULAR_ADD_X{"CoordCircularAddX"};
        static constexpr char const *CFG_COORD_CIRCULAR_ADD_Y{"CoordCircularAddY"};
        static constexpr char const *CFG_COORD_LEFT{"CoordLeft"};
        static constexpr char const *CFG_COORD_TOP{"CoordTop"};
        static constexpr char const *CFG_COORD_RIGHT{"CoordRight"};
        static constexpr char const *CFG_COORD_BOTTOM{"CoordBottom"};
        static constexpr char const *CFG_COORD_CURSOR_X{"CoordCursorX"};
        static constexpr char const *CFG_COORD_CURSOR_Y{"CoordCursorY"};

        static constexpr char const *CFG_RANDOM_SEED{"RandomSeed"};

        static constexpr char const *CFG_TEMPORAL_ANIMATE{"TemporalAnimate"};
        static constexpr char const *CFG_TEMPORAL_START{"TemporalStart"};
        static constexpr char const *CFG_TEMPORAL_END{"TemporalEnd"};
        static constexpr char const *CFG_TEMPORAL_CURRENT{"TemporalCurrent"};
        static constexpr char const *CFG_TEMPORAL_STYLE{"TemporalStyle"};
        static constexpr char const *CFG_TEMPORAL_PERIOD{"TemporalPeriod"};
        static constexpr char const *CFG_TEMPORAL_FPS{"TemporalFps"};

        static constexpr char const *CFG_FUNCTION_STYLE{"FunctionStyle"};

        static constexpr char const *CFG_EXPORT_RESOLUTION{"ExportResolution"};
        static constexpr char const *CFG_EXPORT_SUPERSAMPLING{"ExportSupersampling"};

        static constexpr char const *DEFAULT_CFG =
                "[ColorChooser]\n"
                "TextureWidth=256\n"
                "SimpleColors=false\n"
                "Colors=0 rgb(0,0,0);1 rgb(255,255,255);\n"
                "\n"
                "[Input]\n"
                "Input=\n"
                "ColorStart=-1\n"
                "ColorEnd=1\n"
                "ColorStyle=0\n"
                "CoordStyle=0\n"
                "CoordPolarAddX=1\n"
                "CoordPolarAddY=1\n"
                "CoordCircularAddX=1\n"
                "CoordCircularAddY=1\n"
                "CoordLeft=-1\n"
                "CoordTop=1\n"
                "CoordRight=1\n"
                "CoordBottom=-1\n"
                "CoordCursorX=0\n"
                "CoordCursorY=0\n"
                "RandomSeed=13.153778773876065\n"
                "TemporalStyle=0\n"
                "TemporalAnimate=false\n"
                "TemporalStart=0\n"
                "TemporalEnd=1\n"
                "TemporalCurrent=0\n"
                "TemporalPeriod=1\n"
                "TemporalFps=60\n"
                "FunctionStyle=0\n"
                "ExportResolution=1920x1080\n"
                "ExportSupersampling=1";
    } // namespace cfg

    /// User interface specification namespace.
    namespace ui
    {
        static constexpr char const *workArea =
                "<interface>"
                "<object class=\"GtkVBox\" id=\"work_area\">\n"
                "  <property name=\"homogeneous\">false</property>\n"
                "  <property name=\"spacing\">5</property>\n"
                "  <child>\n"
                "  <object class=\"GtkHBox\">\n"
                "        <child>\n"
                "          <object class=\"GtkPaned\">\n"
                "            <property name=\"orientation\">vertical</property>\n"
                "            <property name=\"position\">500</property>\n"
                "            <child>\n"
                "              <object class=\"GtkGLArea\" id=\"draw_area\">\n"
                "              </object>\n"
                "              <packing>\n"
                "                <property name=\"resize\">True</property>\n"
                "                <property name=\"shrink\">True</property>\n"
                "              </packing>\n"
                "            </child>\n"
                "            <child>\n"
                "              <object class=\"GtkScrolledWindow\">\n"
                "                <property name=\"height_request\">100</property>\n"
                "                <child>\n"
                "                  <object class=\"GtkTextView\" id=\"input\">\n"
                "                    <property name=\"editable\">True</property>\n"
                "                    <style><class name=\"input\"/></style>\n"
                "                  </object>\n"
                "                </child>\n"
                "              </object>\n"
                "              <packing>\n"
                "                <property name=\"resize\">True</property>\n"
                "                <property name=\"shrink\">False</property>\n"
                "              </packing>\n"
                "            </child>\n"
                "          </object>\n"
                "          <packing>\n"
                "            <property name=\"expand\">True</property>\n"
                "            <property name=\"fill\">True</property>\n"
                "          </packing>\n"
                "        </child>\n"
                "        <child>\n"
                "          <object class=\"GtkSeparator\">\n"
                "          </object>\n"
                "          <packing>\n"
                "            <property name=\"expand\">False</property>\n"
                "            <property name=\"fill\">False</property>\n"
                "          </packing>\n"
                "        </child>\n"
                "        <child>\n"
                "          <object class=\"GtkNotebook\">\n"
                "            <property name=\"width_request\">200</property>\n"
                "            <child>\n"
                "              <object class=\"GtkLabel\">\n"
                "                <property name=\"label\">Content</property>\n"
                "              </object>\n"
                "            </child>\n"
                "            <child type=\"tab\">\n"
                "              <object class=\"GtkLabel\">\n"
                "                <property name=\"label\">Basic</property>\n"
                "              </object>\n"
                "            </child>"
                "          </object>\n"
                "          <packing>\n"
                "            <property name=\"expand\">False</property>\n"
                "            <property name=\"fill\">False</property>\n"
                "          </packing>\n"
                "        </child>\n"
                "  </object>"
                "  <packing>\n"
                "    <property name=\"expand\">True</property>\n"
                "    <property name=\"fill\">True</property>\n"
                "  </packing>\n"
                "  </child>"
                "  <child>\n"
                "    <object class=\"GtkScrolledWindow\">\n"
                "      <property name=\"max_content_height\">90</property>\n"
                "      <property name=\"height_request\">90</property>\n"
                "      <child>\n"
                "        <object class=\"GtkTextView\" id=\"log\">\n"
                "          <property name=\"height_request\">90</property>\n"
                "          <property name=\"editable\">False</property>\n"
                "        </object>\n"
                "      </child>\n"
                "    </object>\n"
                "    <packing>\n"
                "      <property name=\"expand\">False</property>\n"
                "      <property name=\"fill\">False</property>\n"
                "    </packing>\n"
                "  </child>\n"
                "</object>"
                "</interface>";
        static constexpr char const *workAreaAlt =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                        "<!-- Generated with glade 3.20.0 -->\n"
                        "<interface>\n"
                        "  <requires lib=\"gtk+\" version=\"3.20\"/>\n"
                        "  <object class=\"GtkAdjustment\" id=\"adjustment1\">\n"
                        "    <property name=\"upper\">100</property>\n"
                        "    <property name=\"step_increment\">1</property>\n"
                        "    <property name=\"page_increment\">10</property>\n"
                        "  </object>\n"
                        "  <object class=\"GtkAdjustment\" id=\"adjustment2\">\n"
                        "    <property name=\"upper\">100</property>\n"
                        "    <property name=\"step_increment\">1</property>\n"
                        "    <property name=\"page_increment\">10</property>\n"
                        "  </object>\n"
                        "  <object class=\"GtkTextBuffer\" id=\"textbuffer1\"/>\n"
                        "  <object class=\"GtkTextBuffer\" id=\"textbuffer2\"/>\n"
                        "  <object class=\"GtkBox\" id=\"work_area\">\n"
                        "    <property name=\"can_focus\">False</property>\n"
                        "    <property name=\"orientation\">vertical</property>\n"
                        "    <property name=\"spacing\">5</property>\n"
                        "    <child>\n"
                        "      <object class=\"GtkBox\">\n"
                        "        <property name=\"can_focus\">False</property>\n"
                        "        <child>\n"
                        "          <object class=\"GtkPaned\">\n"
                        "            <property name=\"can_focus\">False</property>\n"
                        "            <property name=\"orientation\">vertical</property>\n"
                        "            <property name=\"position\">500</property>\n"
                        "            <child>\n"
                        "              <object class=\"GtkGLArea\" id=\"draw_area\">\n"
                        "              </object>\n"
                        "              <packing>\n"
                        "                <property name=\"resize\">True</property>\n"
                        "                <property name=\"shrink\">True</property>\n"
                        "              </packing>\n"
                        "            </child>\n"
                        "            <child>\n"
                        "              <object class=\"GtkBox\">\n"
                        "                <property name=\"visible\">True</property>\n"
                        "                <property name=\"can_focus\">False</property>\n"
                        "                <property name=\"orientation\">vertical</property>\n"
                        "                <child>\n"
                        "                  <object class=\"GtkScale\">\n"
                        "                    <property name=\"visible\">True</property>\n"
                        "                    <property name=\"can_focus\">True</property>\n"
                        "                    <property name=\"adjustment\">adjustment2</property>\n"
                        "                    <property name=\"round_digits\">1</property>\n"
                        "                    <property name=\"draw_value\">False</property>\n"
                        "                  </object>\n"
                        "                  <packing>\n"
                        "                    <property name=\"expand\">False</property>\n"
                        "                    <property name=\"fill\">True</property>\n"
                        "                    <property name=\"position\">0</property>\n"
                        "                  </packing>\n"
                        "                </child>\n"
                        "                <child>\n"
                        "                  <object class=\"GtkScrolledWindow\">\n"
                        "                    <property name=\"height_request\">100</property>\n"
                        "                    <property name=\"can_focus\">False</property>\n"
                        "                    <child>\n"
                        "                      <object class=\"GtkTextView\" id=\"input\">\n"
                        "                        <property name=\"can_focus\">True</property>\n"
                        "                        <property name=\"buffer\">textbuffer1</property>\n"
                        "                        <style>\n"
                        "                          <class name=\"input\"/>\n"
                        "                        </style>\n"
                        "                      </object>\n"
                        "                    </child>\n"
                        "                  </object>\n"
                        "                  <packing>\n"
                        "                    <property name=\"expand\">True</property>\n"
                        "                    <property name=\"fill\">True</property>\n"
                        "                    <property name=\"position\">1</property>\n"
                        "                  </packing>\n"
                        "                </child>\n"
                        "              </object>\n"
                        "              <packing>\n"
                        "                <property name=\"resize\">True</property>\n"
                        "                <property name=\"shrink\">False</property>\n"
                        "              </packing>\n"
                        "            </child>\n"
                        "          </object>\n"
                        "          <packing>\n"
                        "            <property name=\"expand\">True</property>\n"
                        "            <property name=\"fill\">True</property>\n"
                        "            <property name=\"position\">0</property>\n"
                        "          </packing>\n"
                        "        </child>\n"
                        "        <child>\n"
                        "          <object class=\"GtkSeparator\">\n"
                        "            <property name=\"can_focus\">False</property>\n"
                        "          </object>\n"
                        "          <packing>\n"
                        "            <property name=\"expand\">False</property>\n"
                        "            <property name=\"fill\">False</property>\n"
                        "            <property name=\"position\">1</property>\n"
                        "          </packing>\n"
                        "        </child>\n"
                        "        <child>\n"
                        "          <object class=\"GtkBox\">\n"
                        "            <property name=\"visible\">True</property>\n"
                        "            <property name=\"can_focus\">False</property>\n"
                        "            <property name=\"orientation\">vertical</property>\n"
                        "            <child>\n"
                        "              <object class=\"GtkHeaderBar\">\n"
                        "                <property name=\"visible\">True</property>\n"
                        "                <property name=\"can_focus\">False</property>\n"
                        "                <property name=\"title\">Settings</property>\n"
                        "                <child>\n"
                        "                  <placeholder/>\n"
                        "                </child>\n"
                        "              </object>\n"
                        "              <packing>\n"
                        "                <property name=\"expand\">False</property>\n"
                        "                <property name=\"fill\">True</property>\n"
                        "                <property name=\"position\">0</property>\n"
                        "              </packing>\n"
                        "            </child>\n"
                        "            <child>\n"
                        "              <object class=\"GtkScrolledWindow\">\n"
                        "                <property name=\"visible\">True</property>\n"
                        "                <property name=\"can_focus\">True</property>\n"
                        "                <property name=\"hscrollbar_policy\">never</property>\n"
                        "                <property name=\"vscrollbar_policy\">always</property>\n"
                        "                <property name=\"shadow_type\">in</property>\n"
                        "                <child>\n"
                        "                  <object class=\"GtkViewport\">\n"
                        "                    <property name=\"width_request\">250</property>\n"
                        "                    <property name=\"visible\">True</property>\n"
                        "                    <property name=\"can_focus\">False</property>\n"
                        "                    <property name=\"margin_right\">5</property>\n"
                        "                    <child>\n"
                        "                      <object class=\"GtkBox\">\n"
                        "                        <property name=\"visible\">True</property>\n"
                        "                        <property name=\"can_focus\">False</property>\n"
                        "                        <property name=\"orientation\">vertical</property>\n"
                        "                        <child>\n"
                        "                          <object class=\"GtkExpander\">\n"
                        "                            <property name=\"visible\">True</property>\n"
                        "                            <property name=\"can_focus\">True</property>\n"
                        "                            <child>\n"
                        "                              <object class=\"GtkFrame\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label_xalign\">0</property>\n"
                        "                                <child>\n"
                        "                                  <object class=\"GtkGrid\">\n"
                        "                                    <property name=\"visible\">True</property>\n"
                        "                                    <property name=\"can_focus\">False</property>\n"
                        "                                    <property name=\"column_spacing\">5</property>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Filter</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">1</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton2\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Nearest</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton1</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">1</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton1\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Linear</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton2</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">2</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                  </object>\n"
                        "                                </child>\n"
                        "                                <child type=\"label_item\">\n"
                        "                                  <placeholder/>\n"
                        "                                </child>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                            <child type=\"label\">\n"
                        "                              <object class=\"GtkLabel\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label\" translatable=\"yes\">Color</property>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                          </object>\n"
                        "                          <packing>\n"
                        "                            <property name=\"expand\">False</property>\n"
                        "                            <property name=\"fill\">True</property>\n"
                        "                            <property name=\"position\">0</property>\n"
                        "                          </packing>\n"
                        "                        </child>\n"
                        "                        <child>\n"
                        "                          <object class=\"GtkExpander\">\n"
                        "                            <property name=\"visible\">True</property>\n"
                        "                            <property name=\"can_focus\">True</property>\n"
                        "                            <child>\n"
                        "                              <object class=\"GtkFrame\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label_xalign\">0</property>\n"
                        "                                <child>\n"
                        "                                  <object class=\"GtkGrid\">\n"
                        "                                    <property name=\"visible\">True</property>\n"
                        "                                    <property name=\"can_focus\">False</property>\n"
                        "                                    <property name=\"column_spacing\">5</property>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"halign\">start</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Type</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton3\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Cartesian</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"halign\">start</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton4</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">3</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Polar</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"halign\">start</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton3</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">3</property>\n"
                        "                                        <property name=\"top_attach\">1</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton4\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Circular \n"
                        "Inversion</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"halign\">start</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton3</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">3</property>\n"
                        "                                        <property name=\"top_attach\">2</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                  </object>\n"
                        "                                </child>\n"
                        "                                <child type=\"label_item\">\n"
                        "                                  <placeholder/>\n"
                        "                                </child>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                            <child type=\"label\">\n"
                        "                              <object class=\"GtkLabel\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label\" translatable=\"yes\">Coordinate space</property>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                          </object>\n"
                        "                          <packing>\n"
                        "                            <property name=\"expand\">False</property>\n"
                        "                            <property name=\"fill\">True</property>\n"
                        "                            <property name=\"position\">1</property>\n"
                        "                          </packing>\n"
                        "                        </child>\n"
                        "                        <child>\n"
                        "                          <object class=\"GtkExpander\">\n"
                        "                            <property name=\"visible\">True</property>\n"
                        "                            <property name=\"can_focus\">True</property>\n"
                        "                            <child>\n"
                        "                              <object class=\"GtkFrame\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label_xalign\">0</property>\n"
                        "                                <child>\n"
                        "                                  <object class=\"GtkGrid\">\n"
                        "                                    <property name=\"visible\">True</property>\n"
                        "                                    <property name=\"can_focus\">False</property>\n"
                        "                                    <property name=\"column_spacing\">5</property>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Seed</property>\n"
                        "                                        <property name=\"justify\">right</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkSpinButton\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"input_purpose\">number</property>\n"
                        "                                        <property name=\"digits\">2</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                  </object>\n"
                        "                                </child>\n"
                        "                                <child type=\"label_item\">\n"
                        "                                  <placeholder/>\n"
                        "                                </child>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                            <child type=\"label\">\n"
                        "                              <object class=\"GtkLabel\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label\" translatable=\"yes\">Random</property>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                          </object>\n"
                        "                          <packing>\n"
                        "                            <property name=\"expand\">False</property>\n"
                        "                            <property name=\"fill\">True</property>\n"
                        "                            <property name=\"position\">2</property>\n"
                        "                          </packing>\n"
                        "                        </child>\n"
                        "                        <child>\n"
                        "                          <object class=\"GtkExpander\">\n"
                        "                            <property name=\"visible\">True</property>\n"
                        "                            <property name=\"can_focus\">True</property>\n"
                        "                            <child>\n"
                        "                              <object class=\"GtkFrame\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label_xalign\">0</property>\n"
                        "                                <child>\n"
                        "                                  <object class=\"GtkGrid\">\n"
                        "                                    <property name=\"visible\">True</property>\n"
                        "                                    <property name=\"can_focus\">False</property>\n"
                        "                                    <property name=\"column_spacing\">5</property>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Animate</property>\n"
                        "                                        <property name=\"justify\">right</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkSwitch\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Start</property>\n"
                        "                                        <property name=\"justify\">right</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">1</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkSpinButton\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"climb_rate\">0.050000000000000003</property>\n"
                        "                                        <property name=\"digits\">1</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">1</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">End</property>\n"
                        "                                        <property name=\"justify\">right</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">2</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkSpinButton\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"digits\">1</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">2</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Type</property>\n"
                        "                                        <property name=\"justify\">right</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">3</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton5\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Linear</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton6</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">3</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton6\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Periodic</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton5</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">4</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                  </object>\n"
                        "                                </child>\n"
                        "                                <child type=\"label_item\">\n"
                        "                                  <placeholder/>\n"
                        "                                </child>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                            <child type=\"label\">\n"
                        "                              <object class=\"GtkLabel\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label\" translatable=\"yes\">Temporal</property>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                          </object>\n"
                        "                          <packing>\n"
                        "                            <property name=\"expand\">False</property>\n"
                        "                            <property name=\"fill\">True</property>\n"
                        "                            <property name=\"position\">3</property>\n"
                        "                          </packing>\n"
                        "                        </child>\n"
                        "                        <child>\n"
                        "                          <object class=\"GtkExpander\">\n"
                        "                            <property name=\"visible\">True</property>\n"
                        "                            <property name=\"can_focus\">True</property>\n"
                        "                            <child>\n"
                        "                              <object class=\"GtkFrame\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label_xalign\">0</property>\n"
                        "                                <child>\n"
                        "                                  <object class=\"GtkGrid\">\n"
                        "                                    <property name=\"visible\">True</property>\n"
                        "                                    <property name=\"can_focus\">False</property>\n"
                        "                                    <property name=\"column_spacing\">5</property>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkLabel\">\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">False</property>\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Mode</property>\n"
                        "                                        <property name=\"justify\">right</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">1</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton10\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Simple</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton7</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">0</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton7\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Multiple</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton8</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">1</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton8\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Orbit</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton7</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">2</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <object class=\"GtkRadioButton\" id=\"radiobutton9\">\n"
                        "                                        <property name=\"label\" translatable=\"yes\">Advanced</property>\n"
                        "                                        <property name=\"visible\">True</property>\n"
                        "                                        <property name=\"can_focus\">True</property>\n"
                        "                                        <property name=\"receives_default\">False</property>\n"
                        "                                        <property name=\"active\">True</property>\n"
                        "                                        <property name=\"draw_indicator\">True</property>\n"
                        "                                        <property name=\"group\">radiobutton7</property>\n"
                        "                                      </object>\n"
                        "                                      <packing>\n"
                        "                                        <property name=\"left_attach\">2</property>\n"
                        "                                        <property name=\"top_attach\">3</property>\n"
                        "                                      </packing>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                    <child>\n"
                        "                                      <placeholder/>\n"
                        "                                    </child>\n"
                        "                                  </object>\n"
                        "                                </child>\n"
                        "                                <child type=\"label_item\">\n"
                        "                                  <placeholder/>\n"
                        "                                </child>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                            <child type=\"label\">\n"
                        "                              <object class=\"GtkLabel\">\n"
                        "                                <property name=\"visible\">True</property>\n"
                        "                                <property name=\"can_focus\">False</property>\n"
                        "                                <property name=\"label\" translatable=\"yes\">Function</property>\n"
                        "                              </object>\n"
                        "                            </child>\n"
                        "                          </object>\n"
                        "                          <packing>\n"
                        "                            <property name=\"expand\">False</property>\n"
                        "                            <property name=\"fill\">True</property>\n"
                        "                            <property name=\"position\">4</property>\n"
                        "                          </packing>\n"
                        "                        </child>\n"
                        "                      </object>\n"
                        "                    </child>\n"
                        "                  </object>\n"
                        "                </child>\n"
                        "              </object>\n"
                        "              <packing>\n"
                        "                <property name=\"expand\">True</property>\n"
                        "                <property name=\"fill\">True</property>\n"
                        "                <property name=\"position\">1</property>\n"
                        "              </packing>\n"
                        "            </child>\n"
                        "          </object>\n"
                        "          <packing>\n"
                        "            <property name=\"expand\">False</property>\n"
                        "            <property name=\"fill\">True</property>\n"
                        "            <property name=\"position\">2</property>\n"
                        "          </packing>\n"
                        "        </child>\n"
                        "      </object>\n"
                        "      <packing>\n"
                        "        <property name=\"expand\">True</property>\n"
                        "        <property name=\"fill\">True</property>\n"
                        "        <property name=\"position\">0</property>\n"
                        "      </packing>\n"
                        "    </child>\n"
                        "    <child>\n"
                        "      <object class=\"GtkScrolledWindow\">\n"
                        "        <property name=\"height_request\">90</property>\n"
                        "        <property name=\"can_focus\">False</property>\n"
                        "        <property name=\"max_content_height\">90</property>\n"
                        "        <child>\n"
                        "          <object class=\"GtkTextView\" id=\"log\">\n"
                        "            <property name=\"height_request\">90</property>\n"
                        "            <property name=\"can_focus\">False</property>\n"
                        "            <property name=\"editable\">False</property>\n"
                        "            <property name=\"buffer\">textbuffer2</property>\n"
                        "          </object>\n"
                        "        </child>\n"
                        "      </object>\n"
                        "      <packing>\n"
                        "        <property name=\"expand\">False</property>\n"
                        "        <property name=\"fill\">False</property>\n"
                        "        <property name=\"position\">1</property>\n"
                        "      </packing>\n"
                        "    </child>\n"
                        "  </object>\n"
                        "</interface>";
    } // namespace ui
} // namespace mw

#endif //QUANTIZER_WORKAREA_H
