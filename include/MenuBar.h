/**
 * @file MenuBar.h
 * @author Tomas Polasek
 * @brief Manu on top of the main window.
 */

#ifndef QUANTIZER_MENUBAR_H
#define QUANTIZER_MENUBAR_H

#include "Types.h"
#include "WorkArea.h"

#include <giomm.h>
#include <gtkmm/menubar.h>
#include <gtkmm/builder.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/aboutdialog.h>
#include <gtkmm/main.h>

/// Namespace containing my widget types.
namespace mw
{
    /**
     * Menu bar on top of the main application window.
     */
    class MenuBar : public Gtk::MenuBar
    {
    public:
        /// Type of the signal used for exiting application.
        using type_signal_quit_app = sigc::signal<void, bool>;

        /// Signal triggered when exiting application.
        type_signal_quit_app signal_quit_app();

        /// Constructor.
        static Glib::RefPtr<MenuBar> create();

        /// Set work area object to call when menu items are selected.
        void setWorkArea(WorkArea *workArea);
    private:
        // File menu.
        Gtk::MenuItem *mReset;
        Gtk::MenuItem *mDefault;
        Gtk::MenuItem *mCfgLoad;
        Gtk::MenuItem *mCfgSave;
        Gtk::MenuItem *mImgExport;
        Gtk::MenuItem *mAnimExport;
        Gtk::MenuItem *mQuit;

        // Help menu.
        Gtk::MenuItem *mAbout;

        /// Work area called when actions are triggered.
        WorkArea *mWorkArea;

        /// Config currently in use.
        std::string mCurrentConfig;

        /// Signal used for exiting the application.
        type_signal_quit_app mSignalQuitApp;

        /// Callback when reset menu item is pressed.
        void reset();
        /// Callback when default menu item is pressed.
        void defaultSettings();
        /// Callback when config load menu item is pressed.
        void cfgLoad();
        /// Callback when config save menu item is pressed.
        void cfgSave();
        /// Callback when image export menu item is pressed.
        void imgExport();
        /// Callback when animation export menu item is pressed.
        void animExport();
        /// Callback when quit menu item is pressed.
        void quit();
        /// Callback when about menu item is pressed.
        void about();

        /// Display the error message.
        void displayError(const std::string &message);
    protected:
        /// Allow access to the protected contrustor.
        friend class Gtk::Builder;

        /**
         * Construct menu loaded using the gtk builder.
         * @param object Loaded object.
         * @param builder Builder used to load it.
         */
        MenuBar(BaseObjectType *object,
                const Glib::RefPtr<Gtk::Builder> &builder);
        virtual ~MenuBar();
    }; // class MenuBar

    /// User interface specification namespace.
    namespace ui
    {
        static constexpr char const *menuBar =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                "<!-- Generated with glade 3.20.0 -->\n"
                "<interface>\n"
                "  <requires lib=\"gtk+\" version=\"3.20\"/>\n"
                "  <object class=\"GtkImage\" id=\"gtk-open\">\n"
                "    <property name=\"visible\">True</property>\n"
                "    <property name=\"can_focus\">False</property>\n"
                "    <property name=\"stock\">gtk-open</property>\n"
                "  </object>\n"
                "  <object class=\"GtkImage\" id=\"gtk-refresh\">\n"
                "    <property name=\"visible\">True</property>\n"
                "    <property name=\"can_focus\">False</property>\n"
                "    <property name=\"stock\">gtk-refresh</property>\n"
                "  </object>\n"
                "  <object class=\"GtkImage\" id=\"gtk-save\">\n"
                "    <property name=\"visible\">True</property>\n"
                "    <property name=\"can_focus\">False</property>\n"
                "    <property name=\"stock\">gtk-save</property>\n"
                "  </object>\n"
                "  <object class=\"GtkImage\" id=\"gtk-save-as\">\n"
                "    <property name=\"visible\">True</property>\n"
                "    <property name=\"can_focus\">False</property>\n"
                "    <property name=\"stock\">gtk-save-as</property>\n"
                "  </object>\n"
                "  <object class=\"GtkImage\" id=\"gtk-quit\">\n"
                "    <property name=\"visible\">True</property>\n"
                "    <property name=\"can_focus\">False</property>\n"
                "    <property name=\"stock\">gtk-quit</property>\n"
                "  </object>\n"
                "  <object class=\"GtkImage\" id=\"gtk-missing-image\">\n"
                "    <property name=\"visible\">True</property>\n"
                "    <property name=\"can_focus\">False</property>\n"
                "    <property name=\"stock\">gtk-missing-image</property>\n"
                "  </object>\n"
                "      <object class=\"GtkMenuBar\" id=\"menu\">\n"
                "        <property name=\"visible\">True</property>\n"
                "        <property name=\"can_focus\">False</property>\n"
                "        <child>\n"
                "          <object class=\"GtkMenuItem\">\n"
                "            <property name=\"visible\">True</property>\n"
                "            <property name=\"can_focus\">False</property>\n"
                "            <property name=\"label\" translatable=\"yes\">_File</property>\n"
                "            <property name=\"use_underline\">True</property>\n"
                "            <child type=\"submenu\">\n"
                "              <object class=\"GtkMenu\">\n"
                "                <property name=\"visible\">True</property>\n"
                "                <property name=\"can_focus\">False</property>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"reset\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Reset Config</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-refresh</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"default\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Default Settings</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-refresh</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkSeparatorMenuItem\">\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"cfg_load\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Load Config...</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-open</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"cfg_save\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Save Config...</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-save</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkSeparatorMenuItem\">\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"img_export\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Export Image...</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-save-as</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"anim_export\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Export Animation...</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-save-as</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkSeparatorMenuItem\">\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"quit\">\n"
                "                    <property name=\"label\" translatable=\"yes\">Quit</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"image\">gtk-quit</property>\n"
                "                    <property name=\"use_stock\">False</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "              </object>\n"
                "            </child>\n"
                "          </object>\n"
                "        </child>\n"
                "        <child>\n"
                "          <object class=\"GtkMenuItem\">\n"
                "            <property name=\"visible\">True</property>\n"
                "            <property name=\"can_focus\">False</property>\n"
                "            <property name=\"label\" translatable=\"yes\">_Help</property>\n"
                "            <property name=\"use_underline\">True</property>\n"
                "            <child type=\"submenu\">\n"
                "              <object class=\"GtkMenu\">\n"
                "                <property name=\"visible\">True</property>\n"
                "                <property name=\"can_focus\">False</property>\n"
                "                <child>\n"
                "                  <object class=\"GtkImageMenuItem\" id=\"about\">\n"
                "                    <property name=\"label\">gtk-about</property>\n"
                "                    <property name=\"visible\">True</property>\n"
                "                    <property name=\"can_focus\">False</property>\n"
                "                    <property name=\"use_underline\">True</property>\n"
                "                    <property name=\"use_stock\">True</property>\n"
                "                  </object>\n"
                "                </child>\n"
                "              </object>\n"
                "            </child>\n"
                "          </object>\n"
                "        </child>\n"
                "      </object>\n"
                "</interface>";
    } // namespace ui
} // namespace mw

#endif //QUANTIZER_MENUBAR_H
