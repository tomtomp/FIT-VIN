/**
 * @file ColorSampler.cpp
 * @author Tomas Polasek
 * @brief Simple color sampler with binding to OpenGL.
 */

#ifndef QUANTIZER_COLORSAMPLER_H
#define QUANTIZER_COLORSAMPLER_H

#include "GlTypes.h"
#include "Types.h"

#include "cairomm/surface.h"

/// Namespace for utility functions and classes.
namespace util
{
/// Simple color sampler with binding to OpenGL.
    class ColorSampler
    {
    public:
        /// Default internal format of the texture.
        static constexpr GLint DEFAULT_INTERNAL_FORMAT{GL_RGBA8};
        /// Default format of the input data.
        static constexpr GLenum DEFAULT_FORMAT{GL_BGRA};
        /// Default type of the input data.
        static constexpr GLenum DEFAULT_TYPE{GL_UNSIGNED_BYTE};
        /// Default texture type.
        static constexpr GLenum DEFAULT_TARGET{GL_TEXTURE_1D};
        /// Default magnification filter.
        static constexpr GLint DEFAULT_MAG_FILTER{GL_LINEAR};
        /// Default minification filter.
        static constexpr GLint DEFAULT_MIN_FILTER{GL_LINEAR};
        /// Default wrapping style in x-axis filter.
        static constexpr GLint DEFAULT_WRAP_S{GL_CLAMP_TO_EDGE};
        /// Default wrapping style in y-axis filter.
        static constexpr GLint DEFAULT_WRAP_T{GL_CLAMP_TO_EDGE};

        ColorSampler() = default;

        /// Free all acquired resources.
        ~ColorSampler();

        /**
         * Create a sampler from given Cairo surface. If the surface
         * has the same size as the currently allocated texture then only
         * the texture data will be replaced. This behaviour can be
         * changed by using the forceDestroy paramter.
         * @param surface Filled Cairo surface.
         * @param forceDestroy Force re-creation of the texture.
         */
        void fromCairoSurface(Cairo::RefPtr<Cairo::ImageSurface> surface,
                              bool forceDestroy = false);

        /**
         * Use this color sampler as sampler1D.
         * @param id ID of the uniform.
         * @param unitId ID of the tearget texture unit, e.g. 1.
         * @param textureUnit Enumeration for target texture unit, e.g. GL_TEXTURE1.
         */
        void use(GLint id, GLint unitId, GLenum textureUnit);

        /**
         * Set MAG and MIN filters to given type.
         * @param newType Texture filtering type, e.g. GL_LINEAR.
         */
        void setFilterBoth(GLint newType);

        /// Free resources.
        void destroy();
    private:
        /// Set MAG, MIN, WRAP parameters for bound texture.
        void setTextureParameters();

        /// ID of the created texture.
        GLuint mTextureId{0};

        /// Internal format of the texture.
        GLint mInternalFormat{DEFAULT_INTERNAL_FORMAT};
        /// Format of the input data.
        GLenum mFormat{DEFAULT_FORMAT};
        /// Type of the input data.
        GLenum mType{DEFAULT_TYPE};
        /// Texture type.
        GLenum mTarget{DEFAULT_TARGET};
        /// Magnification filter.
        GLint mMagFilter{DEFAULT_MAG_FILTER};
        /// Minification filter.
        GLint mMinFilter{DEFAULT_MIN_FILTER};
        /// Wrapping style in x-axis filter.
        GLint mWrapS{DEFAULT_WRAP_S};
        /// Wrapping style in y-axis filter.
        GLint mWrapT{DEFAULT_WRAP_T};
        /// Flag for changing parameters, which need texture re-creation.
        bool mParamChanged{true};
        /// Width of the currently allocated texture memory on the GPU.
        GLsizei mCurrentWidth{0};
        /// Height of the currently allocated texture memory on the GPU.
        GLsizei mCurrentHeight{0};
    protected:
    }; // class ColorSampler
}


#endif //QUANTIZER_COLORSAMPLER_H
