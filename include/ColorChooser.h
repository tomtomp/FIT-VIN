/**
 * @file ColorChooser.cpp
 * @author Tomas Polasek
 * @brief Color chooser widget.
 */

#ifndef QUANTIZER_COLORCHOOSER_H
#define QUANTIZER_COLORCHOOSER_H

#include "Types.h"

#include <gtkmm/frame.h>
#include <gtkmm/box.h>
#include <gtkmm/drawingarea.h>
#include <gtkmm/colorchooserdialog.h>

#include <glibmm/keyfile.h>

/// Namespace containing my widget types.
namespace mw
{
    /// Color chooser widget.
    class ColorChooser : public Gtk::Frame
    {
    public:
        /// Type of the signal for colors changing.
        using type_signal_colors_changed = sigc::signal<void, bool>;

        /// Constructor.
        static Glib::RefPtr<ColorChooser> create();

        /**
         * Set width of the texture in pixels.
         * @param newWidth New width of the texture in pixels.
         */
        void setTextureWidth(int newWidth);

        /**
         * Should the simple texture mode be enabled?
         * @param enable True to enable the simple texture mode.
         */
        void setSimpleMode(bool enable);

        /// Get cairo surface, which can be used as a texture.
        Cairo::RefPtr<Cairo::ImageSurface> textureSurface();

        /// Signal accessor for color changing.
        type_signal_colors_changed signal_colors_changed();

        /// Convert value of this color chooser to string.
        std::string str() const;

        /// Load state from string.
        void fromStr(const std::string &str);
    private:
        /// Default first color.
        static constexpr char const *START_FIRST_COLOR{"rgba(0,0,0,255)"};
        /// Default second color.
        static constexpr char const *START_SECOND_COLOR{"rgba(255,255,255,255)"};
        /// Default width of the draw area.
        static constexpr int START_WIDTH{200};
        /// Default height of the draw area.
        static constexpr int START_HEIGHT{60};
        /// Default texture width in pixels.
        static constexpr int START_TEXTURE_WIDTH{256};
        /// Mark border line width.
        static constexpr double MARK_BORDER{2.0};
        /// Radius of the mark.
        static constexpr double MARK_RADIUS{10.0};
        /// Y position of the mark relative to the widget height.
        static constexpr double MARK_Y_SCALE{0.8};
        /// Alpha channel value, used for drawing the marks.
        static constexpr double MARK_TRANSPARENCY{0.6};
        /// How much does the mouse need to move in order to trigger move event.
        static constexpr double MOVE_DEADZONE{10.0};
        /// Format of the texture surface.
        static constexpr Cairo::Format SURFACE_FORMAT{Cairo::Format::FORMAT_ARGB32};
        /// Name of the group for the config file.
        static constexpr char const *CFG_GROUP_NAME{"ColorChooser"};
        /// Name of the attribute for texture width in the config file.
        static constexpr char const *CFG_TEXTURE_WIDTH{"TextureWidth"};
        /// Name of the attribute for simple texture in the config file.
        static constexpr char const *CFG_SIMPLE_TEXTURE{"SimpleColors"};
        /// Name of the attribute for colors in the config file.
        static constexpr char const *CFG_COLORS{"Colors"};

        /// Helper structure for containing individual colors.
        struct ColorMark
        {
            ColorMark(float pos, const Gdk::RGBA &col);
            ColorMark(float pos, const char *col);
            /// Position <0.0, 1.0>.
            float position;
            /// Color of the mark.
            Gdk::RGBA color;
            /// Contrastig color used for the border.
            Gdk::RGBA contrast;
        private:
            /// Calculate the contrasting color.
            void calcContrast();
        };

        /// Helper structure containing information about mark operations.
        struct ColorMarkMove
        {
            /// Has the move started?
            bool started;
            /// Starting x coordinate of the mouse action.
            double xStart;
            /// Starting y coordinate of the mouse action.
            double yStart;
            /// Which mark has been selected.
            uint64_t selected;
            /// Has the mouse moved?
            bool moved;
        } mMarkMove;

        /// Create elements of user interface.
        void createUI();

        /// Draw callback for color sampler.
        bool samplerDraw(const Cairo::RefPtr<Cairo::Context> &cr);

        /**
         * Callback when mouse press is detected.
         * @param event Event triggered.
         * @return Return true to stop spreading the event.
         */
        bool mousePress(GdkEventButton *event);

        /**
         * Callback when mouse release is detected.
         * @param event Event triggered.
         * @return Return true to stop spreading the event.
         */
        bool mouseRelease(GdkEventButton *event);

        /**
         * Get position <0.0, 1.0> from given mouse click position.
         * @param xPos X coordinate of the mouse click.
         * @return Returns position in <0.0, 1.0>.
         */
        float getPosition(double xPos);

        /**
         * Callback when mouse is moved.
         * @param event Event triggered.
         * @return Return true to stop spreading the event.
         */
        bool mouseDrag(GdkEventMotion *event);

        /**
         * Return index of the first color marker coliding
         * with given coordinates.
         * @param x X coordinate.
         * @param y Y coordinate.
         * @return If no collision has been found, returns mColors.size();
         */
        uint64_t colorMarkIndex(double x, double y);

        /**
         * Calculate X coordinate of a mark with given postion.
         * @param position Position of the mark <0.0, 1.0>.
         * @return Returns x coordinate in pixels.
         */
        double markXPos(float position);

        /**
         * Calculate Y coordinate of a mark.
         * @return Returns y coordinate in pixels.
         */
        double markYPos();

        /**
         * Does mark on given position intersect with given point?
         * @param position Position of the mark <0.0, 1.0>.
         * @param x X coordinate of the point.
         * @param y Y coordinate of the point.
         * @return Returns true, if the mark does intersect the point.
         */
        bool markIntersects(float position, double x, double y);

        /**
         * Get distance between 2 points.
         * @return Returns Euclidian distance between the two points.
         */
        double distance(double x1, double y1, double x2, double y2);

        /**
         * Ask the user to specify a color and then return it.
         * @param result Used as color output.
         * @param original The original pre-selected color.
         * @return Returns true, if the user chose a color.
         */
        bool getColor(Gdk::RGBA &result, Gdk::RGBA *original = nullptr);

        /// Redraw widget and notify of color change.
        void changeRedraw();

        /// Recalculate the texture surface.
        void recalcTexture();

        /**
         * Draw the color gradient in given context.
         * @param cr Cairo drawing context.
         * @param width Width of the target.
         * @param height Height of the target.
         */
        void drawGradient(const Cairo::RefPtr<Cairo::Context> &cr, double width, double height);

        /**
         * Draw the simple color texture in given context.
         * @param cr Cairo drawing context.
         * @param width Width of the target.
         * @param height Height of the target.
         */
        void drawSimple(const Cairo::RefPtr<Cairo::Context> &cr, double width, double height);

        /**
         * Draw the color marks in given context.
         * @param cr Cairo drawing context.
         */
        void drawMarks(const Cairo::RefPtr<Cairo::Context> &cr);

        /// List of selected colors.
        std::vector<ColorMark> mColors;

        /// Containing box.
        Gtk::Box mBox;
        /// Drawing the color representation.
        Gtk::DrawingArea mDrawArea;

        /// Width of the texture surface in pixels.
        int mTextureWidth{START_TEXTURE_WIDTH};
        /// Flag signifying texture recalculation is required.
        bool mRecalcTexture{true};
        /// Should the texture contain only the colors without interpolation?
        bool mSimpleTexture{false};
        /// Surface which can be used as a texture.
        Cairo::RefPtr<Cairo::ImageSurface> mTextureSurface;
    protected:
        /// Construct the widget.
        ColorChooser();
        /// Free resources.
        ~ColorChooser();

        /// Signal triggered when colors are changed.
        type_signal_colors_changed mColorsChangedSignal;
    }; // class ColorChooser
}


#endif //QUANTIZER_COLORCHOOSER_H
