#version 330 core

layout(location = 0) in vec3 vertexPosMS;

uniform mat4 mvp;

out vec2 fragPos;

void main()
{
    gl_Position = vec4(vertexPosMS.xy, 1.0, 1.0);
    fragPos = (mvp * gl_Position).xy;
}
