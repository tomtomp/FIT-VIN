What is Quantizer
====

This is a simple application created as a 
part of Computer Art course on the Brno 
University of Technology, Faculty of 
Information technology.

The goal of this application is to provide 
artists an easy way to quantize functions 
and export images/animations of their 
creations.

Using Quantizer
====

There are several ways to start using Quantizer.
The easiest one - if you use Windows OS - is to 
download the distribution package for Windows. 
Other options include compiling the application 
yourself.

Windows
-------

There is already prepared distribution package 
for Windows OS in the **distro** folder.

Linux
-----

There are no prepared distribution packages 
for Linux base operating systems. In order 
to buil this application you will need: 

* C++ compiler which supports C++14
* Gtkmm library version >= 3.0
* Epoxy library (should be included with Gtkmm library)
* Magick++ library + any libraries for supporting image/animation formats

In order to build the application, just 
simply: 

1. **mkdir build && cd build**
2. **cmake ..**
3. **make -j#** - Where **#** is a number 
of threads which should run
4. **cp ../src/basic.fs ../bin/**
5. **cp ../src/basic.vs ../bin/**
6. **cp ../src/HelpWindow.glade ../bin/**
7. **cp ../src/WorkArea.glade ../bin/**
8. Application binary should now be located 
in **bin** folder.

TODO
====

1. Progress bar for generating images/animations.
2. Enable creation of long animations and 
animations with high resolution. The 
problem currently is that each frame has 
Magick::Image allocated and that means 
all of the frames need to be in memory 
(uncompressed). This means that generation 
of long animations runs out of memory.
3. Distribution package for Linux and 
other operating systems.
