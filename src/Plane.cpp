/**
 * @file Plane.cpp
 * @author Tomas Polasek
 * @brief Simple render-able plane for OpenGL.
 */

#include "Plane.h"

namespace util
{
    const GLfloat Plane::VERTEX_BUFFER_DATA[] =
            {
                    // x      y     z
                    -1.0f, -1.0f, 0.0f,
                    1.0f, -1.0f, 0.0f,
                    -1.0f, 1.0f, 0.0f,

                    -1.0f, 1.0f, 0.0f,
                    1.0f, -1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f,
            };

    Plane::Plane() :
            mVAId{0u}
    { }

    void Plane::prepare()
    {
        // Create vertex array.
        glGenVertexArrays(1, &mVAId);
        if (!mVAId)
        {
            throw std::runtime_error("Unable to glGenVertexArrays!");
        }
        glBindVertexArray(mVAId);

        // Create vertex buffer object.
        glGenBuffers(1, &mVBId);
        if (!mVBId)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        glBindBuffer(GL_ARRAY_BUFFER, mVBId);

        // Set the data.
        glBufferData(GL_ARRAY_BUFFER, sizeof(VERTEX_BUFFER_DATA), VERTEX_BUFFER_DATA, GL_STATIC_DRAW);

        // Unbind the buffer and array.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    void Plane::render()
    {
        // Bind the vertex array.
        glBindVertexArray(mVAId);
        // Location 0.
        glEnableVertexAttribArray(0);
        // Bind the data.
        glBindBuffer(GL_ARRAY_BUFFER, mVBId);
        // Set attributes.
        glVertexAttribPointer(
                0,             // Layout location.
                3,             // 3 vertexes for each triangle.
                GL_FLOAT,      // Type of the vertex data.
                GL_FALSE,      // Not normalized.
                0,             // Stride, data are behind each other.
                nullptr        // No offset.
        );

        // Draw the triangles.
        glDrawArrays(GL_TRIANGLES, 0, NUM_VERTICES);

        // Disable the attribute.
        glDisableVertexAttribArray(0);
        // Disable the vertex array.
        glBindVertexArray(mVAId);
    }

    Plane::~Plane()
    { destroy(); }

    void Plane::destroy()
    {
        if (mVAId)
        {
            glDeleteVertexArrays(1, &mVAId);
            mVAId = 0;
        }

        if (mVBId)
        {
            glDeleteBuffers(1, &mVBId);
            mVBId = 0;
        }
    }
}
