/**
 * @file ColorChooser.cpp
 * @author Tomas Polasek
 * @brief Color chooser widget.
 */

#include "ColorChooser.h"

namespace mw
{
    Glib::RefPtr<ColorChooser> ColorChooser::create()
    {
        return Glib::RefPtr<ColorChooser>(new ColorChooser);
    }

    void ColorChooser::setTextureWidth(int newWidth)
    {
        mTextureWidth = newWidth;
        mRecalcTexture = true;
    }

    void ColorChooser::setSimpleMode(bool enable)
    {
        if (mSimpleTexture != enable)
        {
            mSimpleTexture = enable;
            mRecalcTexture = true;
            changeRedraw();
        }
    }

    Cairo::RefPtr<Cairo::ImageSurface> ColorChooser::textureSurface()
    {
        if (mRecalcTexture)
        {
            recalcTexture();
        }
        return mTextureSurface;
    }

    ColorChooser::type_signal_colors_changed ColorChooser::signal_colors_changed()
    {
        return mColorsChangedSignal;
    }

    std::string ColorChooser::str() const
    {
        /*
         * Save following data:
         *  int mTextureWidth{START_TEXTURE_WIDTH};
         *  bool mSimpleTexture{false};
         *  std::vector<ColorMark> mColors;
         */

        Glib::KeyFile f;

        f.set_integer(CFG_GROUP_NAME, CFG_TEXTURE_WIDTH, mTextureWidth);
        f.set_boolean(CFG_GROUP_NAME, CFG_SIMPLE_TEXTURE, mSimpleTexture);

        std::vector<Glib::ustring> colors;

        for (auto &col : mColors)
        {
            std::stringstream ss;
            ss << col.position << " " << col.color.to_string();
            colors.emplace_back(ss.str());
        }

        f.set_string_list(CFG_GROUP_NAME, CFG_COLORS, colors);

        return f.to_data();
    }

    void ColorChooser::fromStr(const std::string &str)
    {
        /*
         * Load following data:
         *  int mTextureWidth{START_TEXTURE_WIDTH};
         *  bool mSimpleTexture{false};
         *  std::vector<ColorMark> mColors;
         */

        Glib::KeyFile f;
        f.load_from_data(str);

        setTextureWidth(f.get_integer(CFG_GROUP_NAME, CFG_TEXTURE_WIDTH));
        setSimpleMode(f.get_boolean(CFG_GROUP_NAME, CFG_SIMPLE_TEXTURE));

        mColors.clear();
        auto colors = f.get_string_list(CFG_GROUP_NAME, CFG_COLORS);
        for (auto col : colors)
        {
            std::stringstream ss(col);
            float position{0.0f};
            std::string color;
            ss >> position >> color;
            mColors.emplace_back(position, color.c_str());
        }

        changeRedraw();
    }

    void ColorChooser::createUI()
    {
        mDrawArea.set_events(Gdk::BUTTON_PRESS_MASK
                             | Gdk::BUTTON_RELEASE_MASK
                             | Gdk::POINTER_MOTION_MASK);
        mDrawArea.signal_draw().connect(
                sigc::mem_fun(*this, &ColorChooser::samplerDraw), false);
        mDrawArea.signal_button_press_event().connect(
                sigc::mem_fun(*this, &ColorChooser::mousePress), false);
        mDrawArea.signal_button_release_event().connect(
                sigc::mem_fun(*this, &ColorChooser::mouseRelease), false);
        mDrawArea.signal_motion_notify_event().connect(
                sigc::mem_fun(*this, &ColorChooser::mouseDrag), false);
        mDrawArea.set_size_request(START_WIDTH, START_HEIGHT);
        mBox.pack_start(mDrawArea, true, true, 6);

        set_tooltip_text("Color selector box. Left click the marks to change their color. "
                                 "Right click marks to delete them. Left click the color box "
                                 "to add new mark.");

        add(mBox);
    }

    bool ColorChooser::samplerDraw(const Cairo::RefPtr<Cairo::Context> &cr)
    {
        auto width = mDrawArea.get_width();
        auto height = mDrawArea.get_height();

        if (mSimpleTexture)
        {
            drawSimple(cr, width, height);
        }
        else
        {
            drawGradient(cr, width, height);
        }

        drawMarks(cr);

        return true;
    }

    bool ColorChooser::mousePress(GdkEventButton *event)
    {
        mMarkMove.started = true;
        mMarkMove.xStart = event->x;
        mMarkMove.yStart = event->y;
        mMarkMove.selected = colorMarkIndex(event->x, event->y);
        mMarkMove.moved = false;
        return true;
    }

    bool ColorChooser::mouseRelease(GdkEventButton *event)
    {
        bool isSelected = mMarkMove.selected < mColors.size();

        if (isSelected && !mMarkMove.moved)
        { // Clicked on mark.
            if (event->button == 1)
            {
                ColorMark& mark = mColors[mMarkMove.selected];
                Gdk::RGBA newColor;
                if (getColor(newColor, &mark.color))
                {
                    mark.color = newColor;
                    changeRedraw();
                }
            }
            else if (event->button >= 2)
            {
                mColors.erase(mColors.begin() + mMarkMove.selected);
                changeRedraw();
            }
        }
        else if (isSelected && mMarkMove.moved)
        { // Finishing mark move.
            if (event->button == 1)
            {
                // Sort the color markers.
                std::sort(mColors.begin(), mColors.end(),[](const ColorMark &a, const ColorMark &b) {
                    return a.position < b.position;
                });
                changeRedraw();
            }
        }
        else if (!isSelected && !mMarkMove.moved)
        { // Clicked on gradient.
            if (event->button == 1)
            {
                Gdk::RGBA newColor;
                if (getColor(newColor))
                {
                    mColors.emplace_back(getPosition(event->x), newColor);
                    changeRedraw();
                }
            }
        }

        mMarkMove.started = false;

        return true;
    }

    float ColorChooser::getPosition(double xPos)
    {
        return std::max(0.0, std::min<double>(xPos, mDrawArea.get_width()))
               / mDrawArea.get_width();
    }

    bool ColorChooser::mouseDrag(GdkEventMotion *event)
    {
        if (mMarkMove.started)
        {
            if (distance(mMarkMove.xStart, mMarkMove.yStart, event->x, event->y) > MOVE_DEADZONE)
            {
                mMarkMove.moved = true;
            }

            if (mMarkMove.moved && mMarkMove.selected < mColors.size())
            { // Move the mark...
                mColors[mMarkMove.selected].position = getPosition(event->x);
                // TODO - queue_draw() ?
                changeRedraw();
                //queue_draw();
            }
        }

        return true;
    }

    uint64_t ColorChooser::colorMarkIndex(double x, double y)
    {
        for (decltype(mColors.size()) iii = 0; iii < mColors.size(); ++iii)
        {
            if (markIntersects(mColors[iii].position, x, y))
            {
                return iii;
            }
        }

        return mColors.size();
    }

    double ColorChooser::markXPos(float position)
    {
        double res = mDrawArea.get_width() * position;
        if (res < MARK_RADIUS)
        {
            return MARK_RADIUS;
        }
        else if (res > mDrawArea.get_width() - MARK_RADIUS)
        {
            return mDrawArea.get_width() - MARK_RADIUS;
        }
        else
        {
            return res;
        }
    }

    double ColorChooser::markYPos()
    {
        return mDrawArea.get_height() * MARK_Y_SCALE;
    }

    bool ColorChooser::markIntersects(float position, double x, double y)
    {
        return distance(markXPos(position), markYPos(), x, y) < MARK_RADIUS;
    }

    double ColorChooser::distance(double x1, double y1, double x2, double y2)
    {
        double xDist = x2 - x1;
        double yDist = y2 - y1;
        return std::sqrt(xDist * xDist + yDist * yDist);
    }

    bool ColorChooser::getColor(Gdk::RGBA &result, Gdk::RGBA *original)
    {
        Gtk::ColorChooserDialog dialog("Please choose a color");
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        dialog.set_transient_for(*window);
        if (original)
        {
            dialog.set_rgba(*original);
        }

        auto dialogResult = dialog.run();

        switch (dialogResult)
        {
            case Gtk::RESPONSE_OK:
            {
                result = dialog.get_rgba();
                return true;
            }

            default:
            {
                return false;
            }
        }
    }

    void ColorChooser::changeRedraw()
    {
        queue_draw();
        mColorsChangedSignal.emit(false);
    }

    void ColorChooser::recalcTexture()
    {
        auto surface = Cairo::RefPtr<Cairo::Surface>::cast_dynamic(mTextureSurface);
        auto cr = Cairo::Context::create(surface);

        if (mSimpleTexture)
        {
            drawSimple(cr, mTextureWidth, 1.0);
        }
        else
        {
            drawGradient(cr, mTextureWidth, 1.0);
        }
    }

    void ColorChooser::drawGradient(const Cairo::RefPtr<Cairo::Context> &cr,
                                    double width, double height)
    {
        auto gradient = Cairo::LinearGradient::create(0.0, 0.0, width, 0.0);

        for (auto &color : mColors)
        {
            gradient->add_color_stop_rgb(
                    color.position,
                    color.color.get_red(),
                    color.color.get_green(),
                    color.color.get_blue()
            );
        }

        auto pattern = Cairo::RefPtr<const Cairo::Pattern>::cast_dynamic(gradient);

        cr->set_line_width(0);
        cr->rectangle(0.0, 0.0, width, height);
        cr->set_source(pattern);
        cr->fill();
    }

    void ColorChooser::drawSimple(const Cairo::RefPtr<Cairo::Context> &cr,
                                  double width, double height)
    {
        double lastX{0.0};
        cr->set_line_width(0);

        if (mColors.size() == 1)
        { // Single color, fill the whole image.
            const auto &color = mColors[0];
            cr->rectangle(0.0, 0.0, width, height);
            cr->set_source_rgb(
                color.color.get_red(),
                color.color.get_green(),
                color.color.get_blue()
            );
            cr->fill();
        }
        else if (mColors.size() > 1)
        { // Multiple colors, use advanced drawing.
            // TODO - Use index array and sort it.
            auto colorsCopy = mColors;
            // Sort the color markers.
            std::sort(colorsCopy.begin(), colorsCopy.end(),[](const ColorMark &a, const ColorMark &b) {
                return a.position < b.position;
            });

            for (uint64_t iii = 0; iii < colorsCopy.size(); ++iii)
            {
                const auto &color = colorsCopy[iii];
                if (iii == colorsCopy.size() - 1)
                { // Fill the rest with last color.
                    cr->rectangle(lastX, 0.0, width, height);
                }
                else
                { // Calculate middle between 2 points.
                    float middle = (color.position + colorsCopy[iii + 1].position) / 2.0f;
                    cr->rectangle(lastX, 0.0, middle * width, height);
                    lastX = middle * width;
                }
                cr->set_source_rgb(
                        color.color.get_red(),
                        color.color.get_green(),
                        color.color.get_blue()
                );
                cr->fill();

            }
        }
    }

    void ColorChooser::drawMarks(const Cairo::RefPtr<Cairo::Context> &cr)
    {
        for (auto &color : mColors)
        {
            cr->set_line_width(MARK_BORDER);
            cr->arc(markXPos(color.position), markYPos(), MARK_RADIUS, 0.0, 2.0 * M_PI);
            cr->set_source_rgba(
                    color.color.get_red(),
                    color.color.get_green(),
                    color.color.get_blue(),
                    MARK_TRANSPARENCY
            );
            cr->fill_preserve();
            cr->set_source_rgb(
                    color.contrast.get_red(),
                    color.contrast.get_green(),
                    color.contrast.get_blue()
            );
            cr->stroke();
        }
    }

    ColorChooser::ColorChooser() :
        mBox(Gtk::Orientation::ORIENTATION_VERTICAL, 12),
        mDrawArea(),
        mTextureSurface(Cairo::ImageSurface::create(SURFACE_FORMAT, mTextureWidth, 1))
    {
        mColors.emplace_back(ColorMark(0.0f, START_FIRST_COLOR));
        mColors.emplace_back(ColorMark(1.0f, START_SECOND_COLOR));
        createUI();
    }

    ColorChooser::~ColorChooser()
    {

    }

    ColorChooser::ColorMark::ColorMark(float pos, const Gdk::RGBA &col) :
            position{pos}, color(col)
    {
        calcContrast();
    }

    ColorChooser::ColorMark::ColorMark(float pos, const char *col) :
            position{pos}, color(col)
    {
        calcContrast();
    }

    void ColorChooser::ColorMark::calcContrast()
    {
        // Based on: https://stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color
        double a = 1.0 - (0.299 * color.get_red()
                          + 0.587 * color.get_green()
                          + 0.114 * color.get_blue());

        if (a < 0.5)
        {
            contrast.set_rgba(0.0, 0.0, 0.0, 1.0);
        }
        else
        {
            contrast.set_rgba(1.0, 1.0, 1.0, 1.0);
        }
    }
}
