/**
 * @file Application.cpp
 * @author Tomas Polasek
 * @brief Main application class.
 */

#include "Application.h"

std::string getEnvVar(std::string name)
{
    auto val = std::getenv(name.c_str());
    return val == nullptr ? std::string("<NULL>") : std::string(val);
}

Glib::RefPtr<Application> Application::create()
{
    return Glib::RefPtr<Application>(new Application);
}

Application::Application() :
    Gtk::Application("cz.vut.fit.vin.quantizer")
{
    g_set_application_name("Quantizer");
    /*
    gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(), ".");
    gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(), "./share/");
    gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(), "./share/icons/");
    gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(), "./share/icons/hicolor/");
    gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(), "./share/icons/Adwaita/");
     */
}

Application::~Application()
{

}

void Application::on_startup()
{
    Gtk::Application::on_startup();
}

void Application::on_activate()
{
    Gtk::Application::on_activate();
    createWindow();
}

void Application::createWindow()
{
    auto window = new mw::ApplicationWindow;
    add_window(*window);

    window->signal_hide().connect(sigc::bind<Gtk::Window*>(
            sigc::mem_fun(*this, &Application::on_window_hide), window
    ));
    window->signal_quit_app().connect(
            sigc::mem_fun(*this, &Application::quitApplication));

    window->show_all();
}

void Application::on_window_hide(Gtk::Window *window)
{
    delete window;
}

void Application::quitApplication(bool)
{
    quit();
}
