/**
 * @file ApplicationWindow.cpp
 * @author Tomas Polasek
 * @brief Main application window.
 */

#include "ApplicationWindow.h"

namespace mw
{
    ApplicationWindow::type_signal_quit_app ApplicationWindow::signal_quit_app()
    {
        return mSignalQuitApp;
    }

    ApplicationWindow::ApplicationWindow()
    {
        set_title("Quantizer");
        set_default_size(800, 600);
        set_border_width(10);

        useCustomCss();

        mMenu = MenuBar::create().release();
        Gtk::manage(mMenu);

        mWorkArea = WorkArea::create().release();
        Gtk::manage(mWorkArea);

        //mStatusBar = StatusBar::create().release();
        //Gtk::manage(mStatusBar);

        mMenu->setWorkArea(mWorkArea);
        mMenu->signal_quit_app().connect(
                sigc::mem_fun(*this, &ApplicationWindow::quitApp));

        mMainBox.pack_start(*mMenu, false, false);
        mMainBox.pack_start(*mWorkArea, true, true);
        //mMainBox.pack_start(*mStatusBar, false, false);

        add(mMainBox);
    }

    ApplicationWindow::~ApplicationWindow()
    {

    }

    void ApplicationWindow::quitApp(bool)
    {
        mSignalQuitApp.emit(true);
    }
}
