/**
 * @file StatusBar.h
 * @author Tomas Polasek
 * @brief Status bar on the bottom of the main window.
 */

#ifndef QUANTIZER_STATUSBAR_H
#define QUANTIZER_STATUSBAR_H

#include "Types.h"

#include <gtkmm/statusbar.h>
#include <gtkmm/spinner.h>

/// Namespace containing my widget types.
namespace mw
{
    /**
     * Status bar on the bottom of the window.
     */
    class StatusBar : public Gtk::Statusbar
    {
    public:
        /// Constructor.
        static Glib::RefPtr<StatusBar> create();
    private:
    protected:
        StatusBar();
        virtual ~StatusBar();

        /// Spinner signifying work in progress.
        Gtk::Spinner mWorkSpinner;
    };
    // class StatusBar
} // namespace mw

#endif //QUANTIZER_STATUSBAR_H
