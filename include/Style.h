/**
 * @file Style.h
 * @author Tomas Polasek
 * @brief Style definition for the application.
 */

#ifndef QUANTIZER_STYLE_H_H
#define QUANTIZER_STYLE_H_H

#include "Types.h"

#include <gtkmm/cssprovider.h>
#include <gtkmm/stylecontext.h>

/// Namespace containing my widget types.
namespace mw
{
    /// Namespace containing CSS stye specification.
    namespace css
    {
        /// Global CSS style sheet.
        static constexpr char const *global =
                "#input {"
                "  font-family: serif;"
                "  font-size: 16pt;"
                "}"
                "#input_incorrect {"
                "  font-family: serif;"
                "  font-size: 16pt;"
                "}"
                "#input text {"
                "  color: black;"
                "}"
                "#input_incorrect text {"
                "  color: red;"
                "}"
                "paned.separator {"
                "  background-size: 10px 10px;"
                "}";
    } // namespace css

    /// Use custom CSS style globally.
    inline void useCustomCss()
    {
        auto provider = Gtk::CssProvider::create();
        try {
            provider->load_from_data(css::global);
        } catch (const Gtk::CssProviderError &err) {
            std::cerr << "Unable to load CSS: \n"
                      << err.domain() << "-" << err.code()
                      << "-" << err.what() << std::endl;
            return;
        }

        auto context = Gtk::StyleContext::create();
        auto screen = Gdk::Screen::get_default();
        context->add_provider_for_screen(screen, provider,
                                         GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    }
} // namespace mw

#endif //QUANTIZER_STYLE_H_H
