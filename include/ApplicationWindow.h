/**
 * @file ApplicationWindow.h
 * @author Tomas Polasek
 * @brief Main application window.
 */

#ifndef QUANTIZER_APPLICATIONWINDOW_H
#define QUANTIZER_APPLICATIONWINDOW_H

#include "Types.h"
#include "MenuBar.h"
#include "WorkArea.h"
#include "StatusBar.h"
#include "Style.h"

#include <gtkmm/applicationwindow.h>
#include <gtkmm/hvbox.h>

/// Namespace containing my widget types.
namespace mw
{
    /**
     * Main application window.
     */
    class ApplicationWindow : public Gtk::ApplicationWindow
    {
    public:
        /// Type of the signal used for exiting application.
        using type_signal_quit_app = sigc::signal<void, bool>;

        /// Signal triggered when exiting application.
        type_signal_quit_app signal_quit_app();

        ApplicationWindow();
        virtual ~ApplicationWindow();
    private:
        /// Main window container.
        Gtk::VBox mMainBox;
        /// Menu bar on top.
        mw::MenuBar *mMenu;
        /// Work area in the middle.
        mw::WorkArea *mWorkArea;
        /// Status bar on bottom.
        mw::StatusBar *mStatusBar;

        /// Signal used for exiting the application.
        type_signal_quit_app mSignalQuitApp;

        /// Callback for exiting the application.
        void quitApp(bool);
    protected:
    }; // class ApplicationWindow
} // namespace mw

#endif //QUANTIZER_APPLICATIONWINDOW_H
