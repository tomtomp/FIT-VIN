/**
 * @file main.h
 * @author Tomas Polasek
 * @brief Main function and application construction.
 */

#ifndef QUANTIZER_MAIN_H
#define QUANTIZER_MAIN_H

#include "Application.h"
// ImageMagick C++ library.
#include <Magick++.h>

#endif //QUANTIZER_MAIN_H
