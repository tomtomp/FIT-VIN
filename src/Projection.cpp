/**
 * @file Projection.cpp
 * @author Tomas Polasek
 * @brief Projection matrix for the draw area.
 */

#include "Projection.h"

namespace util
{
    Projection::Projection(glm::vec2 topLeft, glm::vec2 bottomRight):
            mDirty{true},
            mTopLeft{topLeft}, mBottomRight{bottomRight},
            mAspect{0.0}, mScale{0.0},
            mWidth{0}, mHeight{0}
    { }

    GLfloat *Projection::data()
    {
        if (mDirty)
        {
            recalculate();
        }
        return &mMat[0][0];
    }

    const GLfloat *Projection::data() const
    {
        if (mDirty)
        {
            throw std::runtime_error("Unable to recalculate constant Projection!");
        }
        return &mMat[0][0];
    }

    void Projection::setScreenSize(int width, int height)
    {
        mWidth = width;
        mHeight = height;
        mAspect = static_cast<float>(width) / height;
        mDirty = true;
    }

    void Projection::setWindow(glm::vec2 topLeft, glm::vec2 bottomRight)
    {
        mTopLeft = topLeft;
        mBottomRight = bottomRight;
        mDirty = true;
    }

    glm::vec2 Projection::middle() const
    {
        return glm::vec2{
                (mTopLeft.x + mBottomRight.x) / 2.0f,
                (mTopLeft.y + mBottomRight.y) / 2.0f
        };
    }

    glm::vec2 Projection::screenFromMouse(const glm::vec2 &m)
    {
        // Recalculate to <-1.0, 1.0>
        float xIn{(m.x / mWidth) * 2.0f - 1.0f};
        float yIn{(m.y / mHeight) * -2.0f + 1.0f};

        // Calculate screen position.
        /*
        return glm::vec2{
                xIn * mAspect * mScale + middle().x,
                yIn * mScale + middle().y
        };
         */
        return glm::vec2{
                xIn * mat()[0] + mat()[12],
                yIn * mat()[5] + mat()[13]
        };
    }

    void Projection::use(GLint uniformLoc)
    {
        glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, data());
    }

    void Projection::recalculate()
    {
        /*
        mat()[0] = mAspect * mScale;   mat()[1] = 0.0;          mat()[2] = 0.0;  mat()[3] = 0.0;
        mat()[4] = 0.0;                mat()[5] = mScale;       mat()[6] = 0.0;  mat()[7] = 0.0;
        mat()[8] = 0.0;                mat()[9] = 0.0;          mat()[10] = 1.0; mat()[11] = 0.0;
        mat()[12] = middle().x;        mat()[13] = middle().y;  mat()[14] = 0.0; mat()[15] = 1.0;
        */

        GLfloat sizeX = mAspect * (mBottomRight.x - mTopLeft.x) / 2.0f;
        GLfloat startX = middle().x;
        GLfloat sizeY = (mBottomRight.y - mTopLeft.y) / 2.0f;
        GLfloat startY = middle().y;

        mat()[0] = sizeX;        mat()[1] = 0.0;          mat()[2] = 0.0;  mat()[3] = 0.0;
        mat()[4] = 0.0;          mat()[5] = sizeY;        mat()[6] = 0.0;  mat()[7] = 0.0;
        mat()[8] = 0.0;          mat()[9] = 0.0;          mat()[10] = 1.0; mat()[11] = 0.0;
        mat()[12] = startX;      mat()[13] = startY;      mat()[14] = 0.0; mat()[15] = 1.0;

        mDirty = false;
    }
}
