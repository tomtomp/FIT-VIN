/**
 * @file GlTypes.h
 * @author Tomas Polasek
 * @brief Types and includes common to the OpenGL library.
 */

#ifndef QUANTIZER_GLTYPES_H
#define QUANTIZER_GLTYPES_H

// OpenGL includes
#include <epoxy/gl.h>
/*
#ifdef _WIN32
#   include <epoxy/wgl.h>
#else
#   include <epoxy/glx.h>
#endif
 */
// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#endif //QUANTIZER_GLTYPES_H
