/**
 * @file HelpWindow.h
 * @author Tomas Polasek
 * @brief Window containing helpful information about the function input.
 */

#include "HelpWindow.h"

namespace mw
{
    Glib::RefPtr<HelpWindow> HelpWindow::create()
    {
        static auto builder = Gtk::Builder::create();
        try {
            //builder->add_from_string(ui::helpWindow);
            builder->add_from_file("HelpWindow.glade");
        } catch (const Glib::Error &err) {
            std::cerr << "Unable to build WorkArea: \n"
                      << err.what() << std::endl;
        }

        HelpWindow *result{nullptr};
        builder->get_widget_derived("help_window", result);

        return Glib::RefPtr<HelpWindow>(result);
    }

    HelpWindow::HelpWindow(BaseObjectType *object,
                           const Glib::RefPtr<Gtk::Builder> &builder) :
        Gtk::Window(object)
    {
        builder->get_widget("button_close", mButtonClose);
        mButtonClose->signal_clicked().connect(
                sigc::mem_fun(*this, &HelpWindow::hideDialog));
    }

    HelpWindow::~HelpWindow()
    {

    }

    void HelpWindow::hideDialog()
    {
        set_visible(false);
    }
}
