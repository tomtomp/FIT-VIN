/**
 * @file Projection.h
 * @author Tomas Polasek
 * @brief Projection matrix for the draw area.
 */

#ifndef QUANTIZER_PROJECTION_H
#define QUANTIZER_PROJECTION_H

#include "GlTypes.h"
#include "Types.h"

/// Namespace for utility functions and classes.
namespace util
{
    /// Simple orthogonal projection matrix
    class Projection
    {
    public:
        Projection(glm::vec2 topLeft = {-1.0f, 1.0f}, glm::vec2 bottomRight = {1.0f, -1.0f});

        Projection(const Projection &o) = default;
        Projection(Projection &&o) = default;

        Projection &operator=(const Projection &o) = default;
        Projection &operator=(Projection &&o) = default;

        /**
         * Inner matrix pointer getter.
         * @warning May trigger recalculation!
         * @return Returns pointer to he matrix.
         */
        GLfloat *data();

        /**
         * Inner matrix pointer getter.
         * @throws std::runtime_error If recalculation is required,
         *   throws an exception.
         * @return Returns pointer to he matrix.
         */
        const GLfloat *data() const;

        /// Set the scaling factor.
        void setScale(float scale)
        { mScale = scale; mDirty = true; }

        /// Set the size of the screen and calculate aspect ratio.
        void setScreenSize(int width, int height);

        /**
         * Set window coordinate system.
         * @param topLeft Top-left point.
         * @param bottomRight Bottom-right point.
         */
        void setWindow(glm::vec2 topLeft, glm::vec2 bottomRight);

        /// Get middle point of this window.
        glm::vec2 middle() const;

        /// Get screen position from mouse position.
        glm::vec2 screenFromMouse(const glm::vec2 &m);

        /// Perform the zoom function.
        void zoom(float change)
        { mScale *= change; mDirty = true; }

        /// Is the matrix dirty?
        bool isDirty() const
        { return mDirty; }

        /**
         * Use this matrix.
         * @param uniformLoc Location of the matrix uniform.
         */
        void use(GLint uniformLoc);

        /// Recalculate the inner matrix.
        void recalculate();
    private:
        /// Direct access to the matrix array.
        GLfloat *mat()
        { return &mMat[0][0]; }

        /// Inner matrix.
        glm::mat4x4 mMat;
        /// Dirty flag for matrix re-computation.
        bool mDirty;

        /// Top-left point.
        glm::vec2 mTopLeft;
        /// Bottom-right point.
        glm::vec2 mBottomRight;
        /// Aspect ratio.
        float mAspect;
        /// Scaling factor.
        float mScale;
        /// Width of the screen.
        int mWidth;
        /// Height of the screen.
        int mHeight;
    protected:
    }; // class Projection
}

#endif //QUANTIZER_PROJECTION_H
