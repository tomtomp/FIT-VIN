/**
 * @file StatusBar.cpp
 * @author Tomas Polasek
 * @brief Status bar on the bottom of the main window.
 */

#include "StatusBar.h"

namespace mw
{
    Glib::RefPtr<StatusBar> StatusBar::create()
    {
        return Glib::RefPtr<StatusBar>(new StatusBar);
    }

    mw::StatusBar::StatusBar()
    {
        pack_start(mWorkSpinner);
        mWorkSpinner.start();
    }

    mw::StatusBar::~StatusBar()
    {

    }
}
