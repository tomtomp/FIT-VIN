/**
 * @file ColorSampler.h
 * @author Tomas Polasek
 * @brief Simple color sampler with binding to OpenGL.
 */

#include "ColorSampler.h"

namespace util
{
    ColorSampler::~ColorSampler()
    {
        destroy();
    }

    void ColorSampler::fromCairoSurface(Cairo::RefPtr<Cairo::ImageSurface> surface,
                                        bool forceDestroy)
    {
        const unsigned char *data = surface->get_data();
        auto width = surface->get_width();
        auto height = surface->get_height();
        forceDestroy = forceDestroy || mParamChanged;
        // TODO - Detect format/type from the surface?

        if (forceDestroy || width != mCurrentWidth || height != mCurrentHeight)
        { // Re-create the texture.
            destroy();
            glGenTextures(1, &mTextureId);
            glBindTexture(mTarget, mTextureId);
            glTexImage1D(
                    mTarget,
                    0, mInternalFormat,
                    width, 0, mFormat,
                    mType, data
            );
            mCurrentWidth = width;
            mCurrentHeight = height;
            mParamChanged = false;
        }
        else
        { // Replace texture data.
            glBindTexture(mTarget, mTextureId);
            glTexSubImage1D(
                    mTarget, 0,
                    0, width, mFormat,
                    mType, data
            );
        }

        setTextureParameters();
    }

    void ColorSampler::use(GLint id, GLint unitId, GLenum textureUnit)
    {
        glUniform1i(id, unitId);
        glActiveTexture(textureUnit);
        glBindTexture(mTarget, mTextureId);
    }

    void ColorSampler::setFilterBoth(GLint newType)
    {
        mMagFilter = newType;
        mMinFilter = newType;
        setTextureParameters();
    }

    void ColorSampler::setTextureParameters()
    {
        glBindTexture(mTarget, mTextureId);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, mMagFilter);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, mMinFilter);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, mWrapS);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, mWrapT);
    }

    void ColorSampler::destroy()
    {
        if (mTextureId)
        {
            glDeleteTextures(1, &mTextureId);
            mTextureId = 0;
        }
    }
}
