/**
 * @file HelpWindow.h
 * @author Tomas Polasek
 * @brief Window containing helpful information about the function input.
 */

#ifndef QUANTIZER_HELPWINDOW_H
#define QUANTIZER_HELPWINDOW_H

#include "Types.h"

#include <gtkmm/window.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>

/// Namespace containing my widget types.
namespace mw
{
    /// Window containing helpful information about the function input.
    class HelpWindow : public Gtk::Window
    {
    public:
        /// Constructor.
        static Glib::RefPtr<HelpWindow> create();
    private:
        /// Allow access to the private constructor.
        friend class Gtk::Builder;

        /**
         * Construct help window loaded using the gtk builder.
         * @param object Loaded object.
         * @param builder Builder used to load it.
         */
        HelpWindow(BaseObjectType *object,
                   const Glib::RefPtr<Gtk::Builder> &builder);
        /// Destroy the window.
        virtual ~HelpWindow();

        /// Hide this dialog
        void hideDialog();
    protected:
        /// Button which can be used to close the help dialog.
        Gtk::Button *mButtonClose;
    }; // class HelpWindow

    /// User interface specification namespace.
    namespace ui
    {
        static constexpr char const *helpWindow =
                "";
    }
} // namespace mw

#endif //QUANTIZER_HELPWINDOW_H
