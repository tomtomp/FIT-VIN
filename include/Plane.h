/**
 * @file Plane.h
 * @author Tomas Polasek
 * @brief Simple render-able plane for OpenGL.
 */

#ifndef QUANTIZER_PLANE_H
#define QUANTIZER_PLANE_H

#include "GlTypes.h"
#include "Types.h"

/// Namespace for utility functions and classes.
namespace util
{
    /// Renderable plane.
    class Plane
    {
    public:
        /// Construct the Triangle and all the buffers.
        Plane();

        /// Prepare buffers.
        void prepare();

        /// Render the triangle
        void render();

        /// Destroy the buffers.
        ~Plane();

        /// Destroy all the GL buffers.
        void destroy();
    private:
        /// 2 triangles -> 6 vertices.
        static constexpr unsigned int NUM_VERTICES{6};
        static constexpr unsigned int VALS_PER_VERTEX{3};

        /// Vertex data for the Plane.
        static const GLfloat VERTEX_BUFFER_DATA[NUM_VERTICES * VALS_PER_VERTEX];

        /// The vertex array ID.
        GLuint mVAId;
        /// The vertex buffer ID.
        GLuint mVBId;
    protected:
    }; // class Plane
} // namespace util

#endif //QUANTIZER_PLANE_H
