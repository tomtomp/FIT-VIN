/**
 * @file Application.h
 * @author Tomas Polasek
 * @brief Main application class.
 */

#ifndef QUANTIZER_APPLICATION_H
#define QUANTIZER_APPLICATION_H

#include "Types.h"
#include "ApplicationWindow.h"

#include <gtkmm/application.h>

/**
 * Main application class.
 */
class Application : public Gtk::Application
{
public:
    /// Constructor.
    static Glib::RefPtr<Application> create();
private:
    /// Create the main window.
    void createWindow();

    /// Destroy indow when hidden.
    void on_window_hide(Gtk::Window *window);

    /// Callback for exiting the application.
    void quitApplication(bool);
protected:
    /// Initialize application.
    void on_startup() override;
    /// Initialize window.
    void on_activate() override;

    Application();
    virtual ~Application();
}; // class Application

#endif //QUANTIZER_APPLICATION_H
