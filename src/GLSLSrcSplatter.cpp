/**
 * @file GLSLSrcSplatter.h
 * @author Tomas Polasek
 * @brief GLSL source code pre-processor.
 */

#include "GLSLSrcSplatter.h"

namespace util
{
    void GLSLSrcSplatter::loadSrcTemplate(const std::string &tFilename)
    {
        std::ifstream file(tFilename);
        if (!file.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open shader file \"") + tFilename + "\"");
        }

        // Reserve the string size upfront.
        file.seekg(0, std::ios::end);
        mTemplate.reserve(file.tellg());
        file.seekg(0, std::ios::beg);

        // Read the whole file into the string.
        mTemplate.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    }

    void GLSLSrcSplatter::loadSrcTemplate(const char *tSrc)
    {
        mTemplate = tSrc;
    }

    void GLSLSrcSplatter::setMarkValue(std::string mark, const std::string &value)
    {
        mMarkValues[mark] = value;
        mDirty = true;
    }

    void GLSLSrcSplatter::appendMarkValue(std::string mark, const std::string &value)
    {
        mMarkValues[mark] += value;
        mDirty = true;
    }

    void GLSLSrcSplatter::populateMarkMap()
    {
        mMarkValues[ppMarks::DIRECTIVE] = "";
        mMarkValues[ppMarks::COORD_DIRECTIVE] = "";
        mMarkValues[ppMarks::INPUT_DIRECTIVE] = "";
        mMarkValues[ppMarks::SRC] = "";
        mMarkValues[ppMarks::ORB] = "";
        mDirty = true;
    }

    void GLSLSrcSplatter::recalculateSrc()
    {
        if (mDirty)
        {
            mSrc = mTemplate;
            for (auto val : mMarkValues)
            {
                replaceAll(mSrc, val.first, val.second);
            }
            mDirty = false;
        }
    }

    void GLSLSrcSplatter::replaceAll(std::string &src,
                                     const std::string &from,
                                     const std::string &to)
    {
        // Based on http://bits.mdminhazulhaque.io/cpp/find-and-replace-all-occurrences-in-cpp-string.html
        for (std::string::size_type iii = 0;
             (iii = src.find(from, iii)) != std::string::npos;)
        {
            src.replace(iii, from.length(), to);
            iii += to.length();
        }
    }
}
