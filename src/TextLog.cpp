/**
 * @file TextLog.cpp
 * @author Tomas Polasek
 * @brief Text message logging.
 */

#include "TextLog.h"

namespace util
{
    void TextLog::setTextView(Gtk::TextView *textView)
    {
        mTextView = textView;
    }

    const char *TextLog::getMsgPrefix(MsgType type)
    {
        switch (type)
        {
            case MsgType::info:
            {
                return "\n[INFO]: ";
            }
            case MsgType::warning:
            {
                return "\n[WARN]: ";
            }
            case MsgType::error:
            {
                return "\n[ERR]: ";
            }
            case MsgType::critical:
            {
                return "\n[CRIT]: ";
            }
            default:
            {
                return "\n[UNK]: ";
            }
        }
    }
}
