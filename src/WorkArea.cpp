/**
 * @file WorkArea.cpp
 * @author Tomas Polasek
 * @brief Work area in the middle of the main window.
 */

#include "WorkArea.h"

namespace util
{
    std::string getExtension(const std::string &fileName)
    {
        auto it = fileName.find_last_of('.');
        if (it != fileName.npos)
        {
            return fileName.substr(it + 1, fileName.npos);
        }
        else
        {
            return "";
        }
    }
}

namespace mw
{
    const WorkArea::ResolutionValue WorkArea::RESOLUTIONS[] = {
            {1920, 1080},
            {3840, 2160},
            {2560, 1440},
            {1600, 900},
            {1366, 768},
            {1280, 720},
            {1024, 768},
            {800, 600},
            {320, 200},
            {200, 200},
            {400, 400},
            {600, 600},
            {1200, 1200},
            {1600, 1600},
            {2000, 2000},
            {2600, 2600},
            {3200, 3200}
    };

    Glib::RefPtr<WorkArea> WorkArea::create()
    {
        static auto builder = Gtk::Builder::create();
        try {
            //builder->add_from_string(ui::workAreaAlt);
            builder->add_from_file("WorkArea.glade");
        } catch (const Glib::Error &err) {
            std::cerr << "Unable to build WorkArea: \n"
                      << err.what() << std::endl;
        }

        WorkArea *result{nullptr};
        builder->get_widget_derived("work_area", result);

        return Glib::RefPtr<WorkArea>(result);
    }

    void WorkArea::redraw()
    {
        if (!mData.skipRedraw)
        {
            mDrawArea->queue_render();
        }
    }

    std::string WorkArea::getConfig() const
    {
        /* Save the following data:
         *  {"Input"};
         *
         *  {"ColorStart"};
         *  {"ColorEnd"};
         *  {"ColorStyle"};
         *
         *  {"CoordStyle"};
         *  {"CoordPolarAddX"};
         *  {"CoordPolarAddY"};
         *  {"CoordCircularAddX"};
         *  {"CoordCircularAddY"};
         *  {"CoordLeft"};
         *  {"CoordTop"};
         *  {"CoordRight"};
         *  {"CoordBottom"};
         *  {"CoordCursorX"};
         *  {"CoordCursorY"};
         *
         *  {"RandomSeed"};
         *
         *  {"TemporalAnimate"};
         *  {"TemporalStart"};
         *  {"TemporalEnd"};
         *  {"TemporalCurrent"};
         *  {"TemporalStyle"};
         *  {"TemporalPeriod"};
         *  {"TemporalFps"};
         *
         *  {"FunctionStyle"};
         *
         *  {"ExportResolution"};
         *  {"ExportSupersampling"};
         */

        Glib::KeyFile f;
        auto colorChooser = mColorChooser->str();
        f.load_from_data(colorChooser);

        f.set_string(cfg::CFG_GROUP, cfg::CFG_INPUT, mInput->get_buffer()->get_text());

        f.set_double(cfg::CFG_GROUP, cfg::CFG_COLOR_START, mColorStart->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COLOR_END, mColorEnd->get_value());
        if (mColorLinear->get_active())
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_COLOR_STYLE, static_cast<int>(FilterType::LINEAR));
        }
        else
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_COLOR_STYLE, static_cast<int>(FilterType::NEAREST));
        }

        if (mCoordCartesian->get_active())
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_COORD_STYLE, static_cast<int>(CoordType::CARTESIAN));
        }
        else if (mCoordPolar->get_active())
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_COORD_STYLE, static_cast<int>(CoordType::POLAR));
        }
        else
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_COORD_STYLE, static_cast<int>(CoordType::CIRCULAR));
        }
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_POLAR_ADD_X, mCoordPolarAddX->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_POLAR_ADD_Y, mCoordPolarAddY->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_CIRCULAR_ADD_X, mCoordCircularAddX->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_CIRCULAR_ADD_Y, mCoordCircularAddY->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_LEFT, mCoordLeft->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_TOP, mCoordTop->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_RIGHT, mCoordRight->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_BOTTOM, mCoordBottom->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_CURSOR_X, mCoordCursorX->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_COORD_CURSOR_Y, mCoordCursorY->get_value());

        f.set_double(cfg::CFG_GROUP, cfg::CFG_RANDOM_SEED, mRandomSeed->get_value());

        if (mTemporalLinear->get_active())
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_STYLE, static_cast<int>(TemporalType::LINEAR));
        }
        else
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_STYLE, static_cast<int>(TemporalType::PERIODIC));
        }
        f.set_boolean(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_ANIMATE, mTemporalAnimate->get_active());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_START, mTemporalStart->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_END, mTemporalEnd->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_CURRENT, mTemporalCurrent->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_PERIOD, mTemporalPeriod->get_value());
        f.set_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_FPS, mTemporalFps->get_value());

        if (mFunctionSimple->get_active())
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_FUNCTION_STYLE, static_cast<int>(FunctionType::SIMPLE));
        }
        else if (mFunctionOrbit->get_active())
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_FUNCTION_STYLE, static_cast<int>(FunctionType::ORBIT));
        }
        else
        {
            f.set_integer(cfg::CFG_GROUP, cfg::CFG_FUNCTION_STYLE, static_cast<int>(FunctionType::ADVANCED));
        }

        auto it = mExportResolution->get_active();
        if (it && *it)
        {
            f.set_string(cfg::CFG_GROUP, cfg::CFG_EXPORT_RESOLUTION, (*it)[mExportResolutionColumns.mColumnValue]);
        }
        else
        {
            f.set_string(cfg::CFG_GROUP, cfg::CFG_EXPORT_RESOLUTION, RESOLUTIONS[0].str());
        }

        f.set_double(cfg::CFG_GROUP, cfg::CFG_EXPORT_SUPERSAMPLING, mExportSupersampling->get_value());

        return f.to_data();
    }

    void WorkArea::loadConfig(const std::string &cfg)
    {
        mColorChooser->fromStr(cfg);

        Glib::KeyFile f;
        f.load_from_data(cfg);

        mInput->get_buffer()->set_text(f.get_string(cfg::CFG_GROUP, cfg::CFG_INPUT));
        mColorStart->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COLOR_START));
        mColorEnd->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COLOR_END));

        mCoordPolarAddX->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_POLAR_ADD_X));
        mCoordPolarAddY->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_POLAR_ADD_Y));
        mCoordCircularAddX->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_CIRCULAR_ADD_X));
        mCoordCircularAddY->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_CIRCULAR_ADD_Y));
        mCoordLeft->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_LEFT));
        mCoordTop->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_TOP));
        mCoordRight->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_RIGHT));
        mCoordBottom->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_BOTTOM));
        mCoordCursorX->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_CURSOR_X));
        mCoordCursorY->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_COORD_CURSOR_Y));

        mRandomSeed->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_RANDOM_SEED));

        mTemporalStart->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_START));
        mTemporalEnd->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_END));
        timeSpanChanged();
        mTemporalCurrent->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_CURRENT));
        mTemporalPeriod->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_PERIOD));
        mTemporalFps->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_FPS));

        { // Try to find the resolution or fallback to the first one.
            auto resolution = f.get_string(cfg::CFG_GROUP, cfg::CFG_EXPORT_RESOLUTION);
            bool foundRes{false};
            for (uint32_t iii = 0; iii < sizeof(RESOLUTIONS) / sizeof(RESOLUTIONS[0]); ++iii)
            {
                if (RESOLUTIONS[iii].str() == resolution)
                {
                    foundRes = true;
                    mExportResolution->set_active(iii);
                    break;
                }
            }
            if (!foundRes)
            {
                mExportResolution->set_active(0);
            }
        }
        mExportSupersampling->set_value(f.get_double(cfg::CFG_GROUP, cfg::CFG_EXPORT_SUPERSAMPLING));

        int enumVal = f.get_integer(cfg::CFG_GROUP, cfg::CFG_COLOR_STYLE);
        switch (enumVal)
        {
            case static_cast<int>(FilterType::NEAREST):
            {
                mColorNearest->set_active(true);
                break;
            }
            default:
            {
                mColorLinear->set_active(true);
                break;
            }
        }

        enumVal = f.get_integer(cfg::CFG_GROUP, cfg::CFG_COORD_STYLE);
        switch (enumVal)
        {
            case static_cast<int>(CoordType::POLAR):
            {
                mCoordPolar->set_active(true);
                break;
            }
            case static_cast<int>(CoordType::CIRCULAR):
            {
                mCoordCircular->set_active(true);
                break;
            }
            default:
            {
                mCoordCartesian->set_active(true);
                break;
            }
        }

        enumVal = f.get_integer(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_STYLE);
        switch (enumVal)
        {
            case static_cast<int>(TemporalType::PERIODIC):
            {
                mTemporalPeriodic->set_active(true);
                break;
            }
            default:
            {
                mTemporalLinear->set_active(true);
                break;
            }
        }

        mTemporalAnimate->set_active(f.get_boolean(cfg::CFG_GROUP, cfg::CFG_TEMPORAL_ANIMATE));

        enumVal = f.get_integer(cfg::CFG_GROUP, cfg::CFG_FUNCTION_STYLE);
        switch (enumVal)
        {
            case static_cast<int>(FunctionType::ORBIT):
            {
                mFunctionOrbit->set_active(true);
                break;
            }
            case static_cast<int>(FunctionType::ADVANCED):
            {
                mFunctionAdvanced->set_active(true);
                break;
            }
            default:
            {
                mFunctionSimple->set_active(true);
                break;
            }
        }
    }

    WorkArea::WorkArea(BaseObjectType *object,
                       const Glib::RefPtr<Gtk::Builder> &builder) :
        Gtk::VBox(object)
    {
        builder->get_widget("draw_area", mDrawArea);

        mDrawArea->set_events(Gdk::BUTTON_PRESS_MASK
                              | Gdk::BUTTON_MOTION_MASK
                              | Gdk::BUTTON_RELEASE_MASK);
        mDrawArea->signal_realize().connect(
                sigc::mem_fun(*this, &WorkArea::glInit));
        mDrawArea->signal_unrealize().connect(
                sigc::mem_fun(*this, &WorkArea::glDestroy), false);
        mDrawArea->signal_render().connect(
                sigc::mem_fun(*this, &WorkArea::glRender), false);
        mDrawArea->signal_resize().connect(
                sigc::mem_fun(*this, &WorkArea::glResize));
        mDrawArea->signal_button_press_event().connect(
                sigc::mem_fun(*this, &WorkArea::buttonPress));
        mDrawArea->signal_button_press_event().connect(
                sigc::mem_fun(*this, &WorkArea::buttonRelease));
        mDrawArea->signal_motion_notify_event().connect(
                sigc::mem_fun(*this, &WorkArea::mouseMove));
        mDrawArea->signal_scroll_event().connect(
                sigc::mem_fun(*this, &WorkArea::mouseScroll));

        builder->get_widget("input", mInput);
        mInput->set_name("input");
        mInput->get_buffer()->signal_changed().connect(
                sigc::mem_fun(*this, &WorkArea::inputChange));

        builder->get_widget("log", mLog);
        mTextLog.setTextView(mLog);

        builder->get_widget("timeline", mTimeLine);
        mTimeLine->signal_change_value().connect(
                sigc::mem_fun(*this, &WorkArea::timeLineUsed));
        mTimeLine->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::timeLineChanged));
        //builder->get_widget("temporal_timeline", mTemporalTimeLine);

        builder->get_widget("color_box", mColorBox);
        mColorChooser = mw::ColorChooser::create().release();
        Gtk::manage(mColorChooser);
        mColorChooser->signal_colors_changed().connect(
                sigc::mem_fun(*this, &WorkArea::colorsChanged));
        mColorBox->pack_start(*mColorChooser, true, true, 4);

        builder->get_widget("color_start", mColorStart);
        mColorStart->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::colorMapChange));
        builder->get_widget("color_end", mColorEnd);
        mColorEnd->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::colorMapChange));
        builder->get_widget("color_linear", mColorLinear);
        mColorLinear->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::filterTypeChange),
                           FilterType::LINEAR));
        builder->get_widget("color_nearest", mColorNearest);
        mColorLinear->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::filterTypeChange),
                           FilterType::NEAREST));

        builder->get_widget("coord_cartesian", mCoordCartesian);
        mCoordCartesian->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::coordTypeChange),
                           CoordType::CARTESIAN));
        builder->get_widget("coord_polar", mCoordPolar);
        mCoordPolar->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::coordTypeChange),
                           CoordType::POLAR));
        builder->get_widget("coord_circular", mCoordCircular);
        mCoordCircular->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::coordTypeChange),
                           CoordType::CIRCULAR));

        builder->get_widget("coord_polar_add", mCoordPolarAdd);
        builder->get_widget("coord_polar_add_x", mCoordPolarAddX);
        mCoordPolarAddX->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::polarAddChange));
        builder->get_widget("coord_polar_add_y", mCoordPolarAddY);
        mCoordPolarAddY->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::polarAddChange));

        builder->get_widget("coord_circular_add", mCoordCircularAdd);
        builder->get_widget("coord_circular_add_x", mCoordCircularAddX);
        mCoordCircularAddX->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::circularAddChange));
        builder->get_widget("coord_circular_add_y", mCoordCircularAddY);
        mCoordCircularAddY->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::circularAddChange));

        builder->get_widget("coord_left", mCoordLeft);
        mCoordLeft->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::coordWindowChange));
        builder->get_widget("coord_top", mCoordTop);
        mCoordTop->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::coordWindowChange));
        builder->get_widget("coord_right", mCoordRight);
        mCoordRight->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::coordWindowChange));
        builder->get_widget("coord_bottom", mCoordBottom);
        mCoordBottom->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::coordWindowChange));
        builder->get_widget("coord_cursor_x", mCoordCursorX);
        mCoordCursorX->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::cursorChange));
        builder->get_widget("coord_cursor_y", mCoordCursorY);
        mCoordCursorY->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::cursorChange));

        builder->get_widget("random_seed", mRandomSeed);
        mRandomSeed->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::seedChange));
        generateSeed();
        builder->get_widget("random_generate", mRandomGenerate);
        mRandomGenerate->signal_clicked().connect(
                sigc::mem_fun(*this, &WorkArea::generateSeed));

        builder->get_widget("temporal_animate", mTemporalAnimate);
        mTemporalAnimate->property_active().signal_changed().connect(
                sigc::mem_fun(*this, &WorkArea::temporalSwitch));
        builder->get_widget("temporal_start", mTemporalStart);
        mTemporalStart->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::timeSpanChanged));
        builder->get_widget("temporal_end", mTemporalEnd);
        mTemporalEnd->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::timeSpanChanged));
        builder->get_widget("temporal_current", mTemporalCurrent);
        builder->get_widget("temporal_linear", mTemporalLinear);
        mTemporalLinear->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::temporalTypeChange),
                           TemporalType::LINEAR));
        builder->get_widget("temporal_periodic", mTemporalPeriodic);
        mTemporalPeriodic->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::temporalTypeChange),
                           TemporalType::PERIODIC));
        builder->get_widget("temporal_period", mTemporalPeriod);
        mTemporalPeriod->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::timePeriodChanged));
        builder->get_widget("temporal_fps", mTemporalFps);
        mTemporalFps->signal_value_changed().connect(
                sigc::mem_fun(*this, &WorkArea::timeFpsChanged));

        builder->get_widget("function_simple", mFunctionSimple);
        mFunctionSimple->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::functionTypeChange),
                           FunctionType::SIMPLE));
        builder->get_widget("function_orbit", mFunctionOrbit);
        mFunctionOrbit->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::functionTypeChange),
                           FunctionType::ORBIT));
        builder->get_widget("function_advanced", mFunctionAdvanced);
        mFunctionAdvanced->signal_toggled().connect(
                sigc::bind(sigc::mem_fun(*this, &WorkArea::functionTypeChange),
                           FunctionType::ADVANCED));

        builder->get_widget("export_resolution", mExportResolution);
        mExportResolutionModel = Gtk::ListStore::create(mExportResolutionColumns);
        mExportResolution->set_model(mExportResolutionModel);
        uint32_t counter{0};
        for (auto res : RESOLUTIONS)
        {
            auto row = *mExportResolutionModel->append();
            row[mExportResolutionColumns.mColumnId] = counter++;
            row[mExportResolutionColumns.mColumnValue] = res.str();
        }
        mExportResolution->set_active(0);
        mExportResolution->pack_start(mExportResolutionColumns.mColumnValue);
        builder->get_widget("export_supersampling", mExportSupersampling);
        mExportSupersampling->set_value(DEFAULT_SUPERSAMPLING);

        builder->get_widget("function_help", mFunctionHelp);
        mFunctionHelp->signal_clicked().connect(
                sigc::mem_fun(*this, &WorkArea::displayHelp));

        mHelpWindow = mw::HelpWindow::create();
    }

    WorkArea::~WorkArea()
    {
    }

    void WorkArea::glInit()
    {
        mDrawArea->make_current();
        try {
            mDrawArea->throw_if_error();
            // Initialize here:
            glInitInner();
        } catch (const Gdk::GLError &err) {
            std::cerr << "Unable to realize DrawArea: \n"
                      << err.domain() << "-" << err.code()
                      << "-" << err.what() << std::endl;
        } catch (const std::runtime_error &err) {
            std::cerr << "Unable to initialize OpenGL: \n"
                      << err.what() << std::endl;
        }
    }

    void WorkArea::glInitInner()
    {
        //glEnable(GL_MULTISAMPLE_ARB);
        glEnable(GL_MULTISAMPLE);

        mData.glFragSrc.loadSrcTemplate(std::string("basic.fs"));

        try {
            mData.glProgram = util::GLSLProgram{
                    {GL_VERTEX_SHADER,   std::string("basic.vs")},
                    {GL_FRAGMENT_SHADER, mData.glFragSrc.c_src()}
            };
        } catch (const std::runtime_error &err) {
            mTextLog.logMessage(util::MsgType::error, "Unable to compile GLSL program: ");
            mTextLog.logMessage(util::MsgType::error, err.what());
        }

        mData.glProgram.use();
        getUniformLocations();

        auto width = mDrawArea->get_allocated_width();
        auto height = mDrawArea->get_allocated_height();
        mData.glMvp.setScreenSize(width, height);
        mData.glMvp.setWindow({ START_LEFT, START_TOP }, { START_RIGHT, START_BOTTOM });
        mData.glMvp.setScale(START_SCALE);

        try {
            mData.glPlane.prepare();
        } catch (const std::runtime_error &err) {
            mTextLog.logMessage(util::MsgType::error, "Unable to prepare draw plane: ");
            mTextLog.logMessage(util::MsgType::error, err.what());
        }

        mData.glSampler.fromCairoSurface(mColorChooser->textureSurface());

        mData.glTime = 0.0;
        mData.glSeed = static_cast<float>(mRandomSeed->get_value());

        mColorStart->set_value(START_FUN);
        mColorEnd->set_value(END_FUN);

        mCoordLeft->set_value(START_LEFT);
        mCoordTop->set_value(START_TOP);
        mCoordRight->set_value(START_RIGHT);
        mCoordBottom->set_value(START_BOTTOM);

        mData.glAdditional = glm::vec2(0.0f, 0.0f);

        mData.skipRedraw = false;
        mData.skipDraw = false;

        mCoordPolarAddX->set_value(POLAR_ADD_X);
        mCoordPolarAddY->set_value(POLAR_ADD_Y);

        mCoordCircularAddX->set_value(CIRCULAR_ADD_X);
        mCoordCircularAddY->set_value(CIRCULAR_ADD_Y);

        mData.animStyle = TEMPORAL_TYPE;
        mTemporalStart->set_value(TEMPORAL_START);
        mTemporalEnd->set_value(TEMPORAL_END);
        mTemporalCurrent->set_value(TEMPORAL_START);
        mTemporalPeriod->set_value(TEMPORAL_PERIOD);
        mTemporalFps->set_value(TEMPORAL_FPS);

        mData.functionType = FUNCTION_TYPE;
    }

    void WorkArea::getUniformLocations()
    {
        mData.glUPosMvp = mData.glProgram.getUniformLocation("mvp");
        mData.glUColorS = mData.glProgram.getUniformLocation("colorS");
        mData.glUTime = mData.glProgram.getUniformLocation("inputTime");
        mData.glUSeed = mData.glProgram.getUniformLocation("seed");
        mData.glUFunT = mData.glProgram.getUniformLocation("funT");
        mData.glUCursorPos = mData.glProgram.getUniformLocation("cursorPos");
        mData.glUAdditional = mData.glProgram.getUniformLocation("additional");
    }

    void WorkArea::glDestroy()
    {
        mDrawArea->make_current();
        try {
            mDrawArea->throw_if_error();
            // De-initialize here:
            mData.glPlane.destroy();
            mData.glProgram.deleteAll();
        } catch (const Gdk::GLError &err) {
            std::cerr << "Unable to unrealize DrawArea: \n"
                      << err.domain() << "-" << err.code()
                      << "-" << err.what() << std::endl;
        } catch (const std::runtime_error &err) {
            std::cerr << "Unable to de-initialize OpenGL: \n"
                      << err.what() << std::endl;
        }
    }

    bool WorkArea::glRender(const Glib::RefPtr<Gdk::GLContext> &context)
    {
        if (mData.skipDraw)
        {
            // Skip any other handlers.
            return true;
        }

        try {
            mDrawArea->throw_if_error();
            glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
            // Draw commands start here:
            mData.glProgram.use();

            mData.glSampler.use(mData.glUColorS, 0, GL_TEXTURE0);
            mData.glMvp.use(mData.glUPosMvp);

            glUniform1f(mData.glUTime, mData.glTime);
            glUniform1f(mData.glUSeed, mData.glSeed);
            glUniform2f(mData.glUFunT, mData.glFunT.x, mData.glFunT.y);
            glUniform2f(mData.glUCursorPos, mData.glCursorPos.x, mData.glCursorPos.y);
            glUniform2f(mData.glUAdditional, mData.glAdditional.x, mData.glAdditional.y);

            mData.glPlane.render();

            // End of draw commands.
            glFlush();

            return true;
        } catch (const Gdk::GLError &err) {
            std::cerr << "Unable to render: \n"
                      << err.domain() << "-" << err.code()
                      << "-" << err.what() << std::endl;
        } catch (const std::runtime_error &err) {
            mTextLog.logMessage(util::MsgType::error, "Unable to render: ");
            mTextLog.logMessage(util::MsgType::error, err.what());
        }

        return false;
    }

    void WorkArea::glResize(int width, int height)
    {
        mData.glMvp.setScreenSize(width, height);
        // TODO - redraw()?
        //redraw();
    }

    bool WorkArea::buttonPress(GdkEventButton *event)
    {
        auto screenPos = mData.glMvp.screenFromMouse(glm::vec2{event->x, event->y});

        if (event->state & Gdk::CONTROL_MASK)
        {
            mData.mouseDrag = true;
            mData.mouseDragStart = screenPos;
            mData.mouseDragOrig = glm::vec2(mCoordCursorX->get_value(),
                                            mCoordCursorY->get_value());
        }
        else
        {
            mData.mouseDrag = false;
            mData.skipRedraw = true;
            mCoordCursorX->set_value(screenPos.x);
            mData.skipRedraw = false;
            mCoordCursorY->set_value(screenPos.y);
        }
        return true;
    }

    bool WorkArea::buttonRelease(GdkEventButton *event)
    {
        if (mData.mouseDrag)
        {
            auto screenPos = mData.glMvp.screenFromMouse(glm::vec2{event->x, event->y});
            mData.mouseDrag = false;
            auto diff = screenPos - mData.mouseDragStart;
            mCoordCursorX->set_value(mData.mouseDragOrig.x + diff.x);
            mCoordCursorY->set_value(mData.mouseDragOrig.y + diff.y);
        }
        return true;
    }

    bool WorkArea::mouseMove(GdkEventMotion *event)
    {
        auto screenPos = mData.glMvp.screenFromMouse(glm::vec2{event->x, event->y});
        if (mData.mouseDrag)
        {
            auto diff = screenPos - mData.mouseDragStart;
            mCoordCursorX->set_value(mData.mouseDragOrig.x + diff.x);
            mCoordCursorY->set_value(mData.mouseDragOrig.y + diff.y);
        }
        else
        {
            mData.skipRedraw = true;
            mCoordCursorX->set_value(screenPos.x);
            mData.skipRedraw = false;
            mCoordCursorY->set_value(screenPos.y);
        }
        return true;
    }

    bool WorkArea::mouseScroll(GdkEventScroll *event)
    {
        return true;
    }

    void WorkArea::inputChange()
    {
        if (mCheckConn)
        {
            mCheckConn.disconnect();
        }
        mCheckConn = Glib::signal_timeout().connect(
                sigc::mem_fun(*this, &WorkArea::checkInput), WAIT_BEFORE_COMPILE);
    }

    bool WorkArea::checkInput()
    {
        // TODO - Remove!
        //mData.glFragSrc.loadSrcTemplate(std::string("basic.fs"));

        auto text = mInput->get_buffer()->get_text();
        processInput(mData.glFragSrc, text);
        try {
            mData.glProgram.addReplaceShader({{GL_FRAGMENT_SHADER, mData.glFragSrc.c_src()}});
            getUniformLocations();
        } catch (const std::runtime_error &err) {
            mInput->set_name("input_incorrect");
            mTextLog.logMessage(util::MsgType::error, "Unable to compile shader: ");
            mTextLog.logMessage(util::MsgType::error, err.what());
            return false;
        }

        redraw();

        mInput->set_name("input");
        return false;
    }

    void WorkArea::processInput(util::GLSLSrcSplatter &src, Glib::ustring &text)
    {
        if (text.empty())
        {
            src.setMarkValue(util::ppMarks::INPUT_DIRECTIVE, EMPTY_DIRECTIVE);
            return;
        }

        switch (mData.functionType)
        {
            case FunctionType::SIMPLE:
            {
                src.setMarkValue(util::ppMarks::INPUT_DIRECTIVE, INPUT_SIMPLE);
                src.setMarkValue(util::ppMarks::SRC, text);
                break;
            }
            case FunctionType::ORBIT:
            {
                src.setMarkValue(util::ppMarks::INPUT_DIRECTIVE, INPUT_ORBIT);

                // First line contains a function definition -> SRC.
                // Other lines contain orbit definitions which need to be pre-processed.
                bool first{true};
                std::stringstream orbSrc;
                Glib::ustring::size_type pos1{0};
                Glib::ustring::size_type pos2{Glib::ustring::npos};

                while ((pos2 = text.find("\n", pos1 + 1)) != Glib::ustring::npos)
                {
                    if (first)
                    {
                        src.setMarkValue(util::ppMarks::SRC, text.substr(pos1, pos2 - pos1));
                        first = false;
                    }
                    else
                    {
                        orbSrc << "orb = length(val - " << text.substr(pos1, pos2 - pos1)
                               << ");\n"
                               << "pst = orb < pst ? orb : pst;" << std::endl;
                    }

                    pos1 = pos2;
                }

                // Process the remainder.
                if (first)
                { // Only one line.
                    src.setMarkValue(util::ppMarks::SRC, text.substr(pos1, pos2));
                }
                else
                { // At least one orbit.
                    orbSrc << "orb = length(val - " << text.substr(pos1, pos2)
                           << ");\n"
                           << "pst = orb < pst ? orb : pst;" << std::endl;
                    src.setMarkValue(util::ppMarks::ORB, orbSrc.str());
                }

                break;
            }
            case FunctionType::ADVANCED:
            {
                src.setMarkValue(util::ppMarks::INPUT_DIRECTIVE, INPUT_ADVANCED);
                src.setMarkValue(util::ppMarks::SRC, text);
                break;
            }
            default:
            {
                std::cerr << "Function type not implemented!" << std::endl;
                break;
            }
        }
    }

    void WorkArea::colorsChanged(bool)
    {
        mData.glSampler.fromCairoSurface(mColorChooser->textureSurface());
        redraw();
    }

    void WorkArea::filterTypeChange(FilterType newType)
    {
        switch (newType)
        {
            case FilterType::LINEAR:
            {
                if (mColorLinear->get_active())
                {
                    mData.glSampler.setFilterBoth(GL_LINEAR);
                    mColorChooser->setSimpleMode(false);
                }
                break;
            }
            case FilterType::NEAREST:
            {
                if (mColorNearest->get_active())
                {
                    mData.glSampler.setFilterBoth(GL_NEAREST);
                    mColorChooser->setSimpleMode(true);
                }
                break;
            }
            default:
            {
                std::cerr << "Filter type not implemented!" << std::endl;
                break;
            }
        }
    }

    void WorkArea::coordTypeChange(CoordType newType)
    {
        mCoordPolarAdd->set_reveal_child(false);
        mCoordCircularAdd->set_reveal_child(false);
        switch (newType)
        {
            case CoordType::CARTESIAN:
            {
                if (mCoordCartesian->get_active())
                {
                    mData.glFragSrc.setMarkValue(util::ppMarks::COORD_DIRECTIVE,
                                                 COORD_CARTESIAN);
                    checkInput();
                }
                break;
            }
            case CoordType::POLAR:
            {
                if (mCoordPolar->get_active())
                {
                    mCoordPolarAdd->set_reveal_child(true);
                    setPolarAdd();
                    mData.glFragSrc.setMarkValue(util::ppMarks::COORD_DIRECTIVE,
                                                 COORD_POLAR);
                    checkInput();
                }
                break;
            }
            case CoordType::CIRCULAR:
            {
                if (mCoordCircular->get_active())
                {
                    mCoordCircularAdd->set_reveal_child(true);
                    setCircularAdd();
                    mData.glFragSrc.setMarkValue(util::ppMarks::COORD_DIRECTIVE,
                                                 COORD_CIRCULAR);
                    checkInput();
                }
                break;
            }
            default:
            {
                std::cerr << "Coord type not implemented!" << std::endl;
                break;
            }
        }
    }

    void WorkArea::temporalTypeChange(TemporalType newType)
    {
        switch (newType)
        {
            case TemporalType::LINEAR:
            {
                if (mTemporalLinear->get_active())
                {
                    mData.animStyle = TemporalType::LINEAR;
                }
                break;
            }
            case TemporalType::PERIODIC:
            {
                if (mTemporalPeriodic->get_active())
                {
                    mData.animStyle = TemporalType::PERIODIC;
                }
                break;
            }
            default:
            {
                std::cerr << "Temporal type not implemented!" << std::endl;
                break;
            }
        }
    }

    void WorkArea::functionTypeChange(FunctionType newType)
    {
        switch (newType)
        {
            case FunctionType::SIMPLE:
            {
                if (mFunctionSimple->get_active())
                {
                    mData.functionType = FunctionType::SIMPLE;
                }
                break;
            }
            case FunctionType::ORBIT:
            {
                if (mFunctionOrbit->get_active())
                {
                    mData.functionType = FunctionType::ORBIT;
                }
                break;
            }
            case FunctionType::ADVANCED:
            {
                if (mFunctionAdvanced->get_active())
                {
                    mData.functionType = FunctionType::ADVANCED;
                }
                break;
            }
            default:
            {
                std::cerr << "Function type not implemented!" << std::endl;
                break;
            }
        }

        checkInput();
    }

    void WorkArea::coordWindowChange()
    {
        mData.glMvp.setWindow(
                { mCoordLeft->get_value(), mCoordTop->get_value() },
                { mCoordRight->get_value(), mCoordBottom->get_value() }
        );
        redraw();
    }

    void WorkArea::seedChange()
    {
        mData.glSeed = static_cast<float>(mRandomSeed->get_value());
        redraw();
    }

    void WorkArea::generateSeed()
    {
        double lowerBound{mRandomSeed->get_adjustment()->get_lower()};
        double upperBound{mRandomSeed->get_adjustment()->get_upper()};
        if (upperBound == 0)
        {
            upperBound = 5 * mRandomSeed->get_max_length();
        }

        std::uniform_real_distribution<double> u(lowerBound, upperBound);

        mRandomSeed->set_value(u(mData.re));
    }

    void WorkArea::colorMapChange()
    {
        recalcFunTransform();
        redraw();
    }

    void WorkArea::recalcFunTransform()
    {
        float start{static_cast<float>(mColorStart->get_value())};
        float end{static_cast<float>(mColorEnd->get_value())};

        mData.glFunT = glm::vec2{
                1.0f / (end - start),
                start
        };
    }

    void WorkArea::cursorChange()
    {
        mData.glCursorPos = glm::vec2{
                static_cast<float>(mCoordCursorX->get_value()),
                static_cast<float>(mCoordCursorY->get_value())
        };
        redraw();
    }

    void WorkArea::polarAddChange()
    {
        if (mCoordPolar->get_active())
        {
            setPolarAdd();
        }
    }

    void WorkArea::circularAddChange()
    {
        if (mCoordCircular->get_active())
        {
            setCircularAdd();
        }
    }

    void WorkArea::setPolarAdd()
    {
        mData.glAdditional = glm::vec2{
                static_cast<float>(mCoordPolarAddX->get_value()),
                static_cast<float>(mCoordPolarAddY->get_value())
        };
        redraw();
    }

    void WorkArea::setCircularAdd()
    {
        mData.glAdditional = glm::vec2{
                static_cast<float>(mCoordCircularAddX->get_value()),
                static_cast<float>(mCoordCircularAddY->get_value())
        };
        redraw();
    }

    void WorkArea::temporalSwitch()
    {
        if (mTemporalAnimate->get_active())
        {
            setupAnimation();
        }
        else
        {
            disableAnimation();
        }
    }

    void WorkArea::timeSpanChanged()
    {
        auto start = static_cast<float>(mTemporalStart->get_value());
        auto end = static_cast<float>(mTemporalEnd->get_value());
        mData.animStart = start;
        mData.animEnd = end;
        auto adj = mTemporalCurrent->get_adjustment();
        adj->set_lower(start);
        adj->set_upper(end);
        if (mTemporalAnimate->get_active())
        {
            setupAnimation();
        }
    }

    void WorkArea::timePeriodChanged()
    {
        if (mTemporalAnimate->get_active())
        {
            setupAnimation();
        }
    }

    void WorkArea::timeFpsChanged()
    {
        if (mTemporalAnimate->get_active())
        {
            setupAnimation();
        }
    }

    void WorkArea::setupAnimation(bool ui)
    {
        if (ui)
        {
            disableAnimation();
        }

        auto period = static_cast<float>(mTemporalPeriod->get_value());
        auto fps = static_cast<float>(mTemporalFps->get_value());

        mData.animState = mData.animStart;
        mData.animAdvance = (mData.animEnd - mData.animStart) / period / fps;

        if (ui)
        {
            mAnimConn = Glib::signal_timeout().connect(
                    sigc::mem_fun(*this, &WorkArea::advanceAnimationRedraw),
                    static_cast<unsigned int>(MS_IN_S / mTemporalFps->get_value())
            );

            auto adj = mTemporalCurrent->get_adjustment();
            adj->set_value(mData.animState);
        }


        mData.glTime = mData.animState;

        if (ui)
        {
            redraw();
        }
    }

    void WorkArea::disableAnimation()
    {
        if (mAnimConn)
        {
            mAnimConn.disconnect();
        }
    }

    void WorkArea::advanceAnimation(bool setTimeline)
    {
        mData.animState += mData.animAdvance;

        switch (mData.animStyle)
        {
            case TemporalType::LINEAR:
            {
                if (mData.animStart < mData.animEnd ?
                    mData.animState > mData.animEnd :
                    mData.animState < mData.animEnd)
                { // Check if we passed the end animation time.
                    mData.animState = mData.animStart;
                }
                break;
            }
            case TemporalType::PERIODIC:
            {
                if (mData.animStart < mData.animEnd ?
                    mData.animState > mData.animEnd :
                    mData.animState < mData.animEnd)
                { // Check if we passed the end animation time.
                    mData.animState = mData.animEnd;
                    std::swap(mData.animStart, mData.animEnd);
                    mData.animAdvance *= -1.0f;
                }
                break;
            }
            default:
            {
                std::cerr << "Unknown animation style!" << std::endl;
                break;
            }
        }

        mData.glTime = mData.animState;
        if (setTimeline)
        {
            mTimeLine->set_value(mData.animState);
        }
    }

    bool WorkArea::advanceAnimationRedraw()
    {
        advanceAnimation();
        redraw();

        return true;
    }

    bool WorkArea::timeLineUsed(Gtk::ScrollType scroll, double newValue)
    {
        mTemporalAnimate->set_active(false);

        // We want the other handlers to still run!
        return false;
    }

    void WorkArea::timeLineChanged()
    {
        mData.glTime = static_cast<float>(mTimeLine->get_value());
        redraw();
    }

    void WorkArea::displayHelp()
    {
        mHelpWindow->set_visible(true);
        mHelpWindow->present();
    }

    WorkArea::ResolutionValue WorkArea::getCurrentResolution()
    {
        auto it = mExportResolution->get_active();
        if (it && *it)
        {
            return RESOLUTIONS[(*it)[mExportResolutionColumns.mColumnId]];
        }
        return RESOLUTIONS[0];
    }

    void WorkArea::exportImage(const std::string &fileName)
    {
        // Handles for required OpenGL objects.
        // Supersample render buffer.
        GLuint srb{0};
        // Supersample framebuffer.
        GLuint sfbo{0};
        // Resolve texture.
        GLuint rt{0};
        // Resolve framebuffer.
        GLuint rfbo{0};

        mDrawArea->make_current();
        try {
            mDrawArea->throw_if_error();

            auto res = getCurrentResolution();
            auto supersampling = static_cast<GLsizei>(mExportSupersampling->get_value());
            int origWidth = mDrawArea->get_width();
            int origHeight = mDrawArea->get_height();

            // Prepare the required structures.
            prepareExportStructures(srb, sfbo, rt, rfbo, res, supersampling);

            // Switch rendering to the super-sampled framebuffer.
            glBindFramebuffer(GL_FRAMEBUFFER, sfbo);
            glViewport(0, 0, res.width * supersampling, res.height * supersampling);
            mData.glMvp.setScreenSize(res.width * supersampling, res.height * supersampling);

            // Render the image.
            glRender(mDrawArea->get_context());

            // Create buffer for the result image.
            auto data = prepareDataBuffer(res);

            // Resolve the super-sampled framebuffer into the target image size.
            resolveFramebuffer(sfbo, rt, rfbo, res, supersampling, data);

            // Switch rendering back to the original viewport.
            mData.glMvp.setScreenSize(origWidth, origHeight);
            glViewport(0, 0, origWidth, origHeight);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            // Parse the data into image object.
            Magick::Image img(res.width, res.height,
                              "RGBA", MagickCore::StorageType::CharPixel,
                              data.data());
            // Get extension of the file.
            auto ext = util::getExtension(fileName);
            if (ext.empty())
            { // Fallback to the default extension.
                img.magick(DEFAULT_IMAGE_EXTENSION);
                img.write(fileName + "." + DEFAULT_IMAGE_EXTENSION);
            }
            else
            { // Use the given extension.
                img.magick(ext);
                img.write(fileName);
            }
        } catch (const std::runtime_error &err) {
            destroyExportStructures(srb, sfbo, rt, rfbo);
            throw;
        } catch (const Magick::Exception &err) {
            destroyExportStructures(srb, sfbo, rt, rfbo);
            throw std::runtime_error(err.what());
        } catch (...) {
            destroyExportStructures(srb, sfbo, rt, rfbo);
            throw std::runtime_error("Unable to use the OpenGL context!");
        }

        destroyExportStructures(srb, sfbo, rt, rfbo);
    }

    void WorkArea::exportAnimation(const std::string &fileName)
    {
        // Handles for required OpenGL objects.
        // Supersample render buffer.
        GLuint srb{0};
        // Supersample framebuffer.
        GLuint sfbo{0};
        // Resolve texture.
        GLuint rt{0};
        // Resolve framebuffer.
        GLuint rfbo{0};

        // Remember the animation state, also re-enabled rendering in the draw area.
        AnimationSaveState s(this);

        // Setup animation without affecting the GUI.
        setupAnimation(false);

        // Number of frames in this animation.
        auto frames = exportAnimationFrames();
        // How long should each frame stay displayed.
        auto frameTime = exportAnimationFrameTime();

        mDrawArea->make_current();
        try {
            mDrawArea->throw_if_error();

            auto res = getCurrentResolution();
            auto supersampling = static_cast<GLsizei>(mExportSupersampling->get_value());

            // Prepare the required structures.
            prepareExportStructures(srb, sfbo, rt, rfbo, res, supersampling);

            // Setup the rendering to the super-sampled framebuffer.
            glViewport(0, 0, res.width * supersampling, res.height * supersampling);
            mData.glMvp.setScreenSize(res.width * supersampling, res.height * supersampling);
            // Automatic restoration of the viewport.
            ViewportSaveState v(this);

            // Create buffer for the result image.
            auto data = prepareDataBuffer(res);

            /// List of frames in the animation.
            std::vector<Magick::Image> animationFrames;

            for (decltype(frames) iii = 0; iii < frames; ++iii)
            {
                std::cout << "Generating frame: " << iii << " / " << frames << std::endl;
                // Switch rendering to the super-sampled framebuffer.
                glBindFramebuffer(GL_FRAMEBUFFER, sfbo);

                // Render the image.
                glRender(mDrawArea->get_context());

                // Resolve the super-sampled framebuffer into the target image size.
                resolveFramebuffer(sfbo, rt, rfbo, res, supersampling, data);

                // Parse the data into a new frame.
                animationFrames.emplace_back(res.width, res.height,
                                  "RGBA", MagickCore::StorageType::CharPixel,
                                  data.data());
                animationFrames.back().animationDelay(frameTime);
                animationFrames.back().compressType(MagickCore::JPEGCompression);
                animationFrames.back().quality(20);
                animationFrames.back().magick(DEFAULT_IMAGE_EXTENSION);


                // Advance the animation to the next time point.
                advanceAnimation(false);

                /*
                // Disable rendering in the draw area.
                mData.skipDraw = true;
                Gtk::Main::iteration(false);
                mData.skipDraw = false;
                 */
            }

            // Get extension of the file.
            auto ext = util::getExtension(fileName);
            std::string animFileName{fileName};
            if (ext != DEFAULT_ANIMATION_EXTENSION)
            { // Fallback to the default extension.
                animFileName.append(".");
                animFileName.append(DEFAULT_ANIMATION_EXTENSION);
            }

            std::cout << "Generating the final gif file!" << std::endl;
            // Write the animation into selected file.
            Magick::writeImages(animationFrames.begin(),
                                animationFrames.end(),
                                animFileName, true);
            std::cout << "Finished the final gif file!" << std::endl;
        } catch (const std::runtime_error &err) {
            destroyExportStructures(srb, sfbo, rt, rfbo);
            throw;
        } catch (const Magick::Exception &err) {
            destroyExportStructures(srb, sfbo, rt, rfbo);
            throw std::runtime_error(err.what());
        } catch (...) {
            destroyExportStructures(srb, sfbo, rt, rfbo);
            throw std::runtime_error("Unable to use the OpenGL context!");
        }

        destroyExportStructures(srb, sfbo, rt, rfbo);
    }

    std::size_t WorkArea::exportAnimationFrames()
    {
        return static_cast<std::size_t>(mTemporalFps->get_value()
                                        * mTemporalPeriod->get_value()
                                        * (mData.animStyle == TemporalType::PERIODIC ? 2.0 : 1.0));
    }

    std::size_t WorkArea::exportAnimationFrameTime()
    {
        // Minimal value is 2, because less than 2 does not work.
        return std::max<std::size_t>(static_cast<std::size_t>(std::abs(mData.animAdvance)), 2u);
    }

    void WorkArea::prepareExportStructures(GLuint &srb, GLuint &sfbo, GLuint &rt, GLuint &rfbo,
                                           ResolutionValue &res, GLsizei supersample)
    {
        glGetError();
        {
            // Multi-sampled render buffer.
            glGenRenderbuffers(1, &srb);
            glBindRenderbuffer(GL_RENDERBUFFER, srb);
            glRenderbufferStorage(GL_RENDERBUFFER,
                                  GL_RGBA16F,
                                  res.width * supersample,
                                  res.height * supersample);

            // Multi-sampled framebuffer.
            glGenFramebuffers(1, &sfbo);
            glBindFramebuffer(GL_FRAMEBUFFER, sfbo);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                                      GL_COLOR_ATTACHMENT0,
                                      GL_RENDERBUFFER,
                                      srb);

            // Resolve texture used for holding the rendered image.
            glGenTextures(1, &rt);
            glBindTexture(GL_TEXTURE_2D, rt);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
                         res.width, res.height, 0,
                         GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

            // Resolve framebuffer.
            glGenFramebuffers(1, &rfbo);
            glBindFramebuffer(GL_FRAMEBUFFER, rfbo);
            glFramebufferTexture2D(GL_FRAMEBUFFER,
                                   GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_2D,
                                   rt, 0);

            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                throw std::runtime_error("Unable to prepare framebuffer/texture!");
            }
            if (glGetError() != GL_NO_ERROR)
            {
                throw std::runtime_error("Unable to render image, try lowering the supersampling value!");
            }
        }
        glGetError();
    }

    void WorkArea::destroyExportStructures(GLuint &srb, GLuint &sfbo, GLuint &rt, GLuint &rfbo)
    {
        glDeleteFramebuffers(1, &rfbo);
        glDeleteTextures(1, &rt);
        glDeleteFramebuffers(1, &rfbo);
        glDeleteRenderbuffers(1, &srb);
    }

    std::vector<unsigned char> WorkArea::prepareDataBuffer(ResolutionValue &res)
    {
        // Prepare storage for the generated image.
        return std::vector<unsigned char>(res.width * res.height * 4);
    }

    std::vector<unsigned char> WorkArea::resolveFramebuffer(GLuint sfbo, GLuint rt, GLuint rfbo,
                                                            ResolutionValue &res, GLsizei supersample,
                                                            std::vector<unsigned char> &data)
    {
        glGetError();
        {
            // Resolve the super-sampled framebuffer to the target framebuffer.
            glBindFramebuffer(GL_READ_FRAMEBUFFER, sfbo);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rfbo);
            glBlitFramebuffer(0, 0, res.width * supersample, res.height * supersample,
                              0, 0, res.width, res.height,
                              GL_COLOR_BUFFER_BIT, GL_LINEAR);

            // Read the final image from texture.
            glBindTexture(GL_TEXTURE_2D, rt);
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.data());

            if (glGetError() != GL_NO_ERROR)
            {
                throw std::runtime_error("Unable to render image, try lowering the supersampling value!");
            }
        }
        glGetError();

        return data;
    }

    WorkArea::AnimationSaveState::AnimationSaveState(WorkArea *workArea) :
        mWorkArea{workArea},
        mAnimStart{workArea->mData.animStart},
        mAnimEnd{workArea->mData.animEnd},
        mAnimState{workArea->mData.animState},
        mAnimAdvance{workArea->mData.animAdvance}
    { }

    WorkArea::AnimationSaveState::~AnimationSaveState()
    {
        mWorkArea->mData.animStart = mAnimStart;
        mWorkArea->mData.animEnd = mAnimEnd;
        mWorkArea->mData.animState = mAnimState;
        mWorkArea->mData.animAdvance = mAnimAdvance;
        mWorkArea->mData.glTime = mAnimState;
        mWorkArea->mData.skipDraw = false;
    }

    WorkArea::ViewportSaveState::ViewportSaveState(WorkArea *workArea) :
        mWorkArea{workArea}
    {
        mWidth = workArea->mDrawArea->get_width();
        mHeight = workArea->mDrawArea->get_height();
    }

    WorkArea::ViewportSaveState::~ViewportSaveState()
    {
        // Switch rendering back to the original viewport.
        mWorkArea->mData.glMvp.setScreenSize(mWidth, mHeight);
        glViewport(0, 0, mWidth, mHeight);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}
