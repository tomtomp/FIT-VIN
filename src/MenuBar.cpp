/**
 * @file MenuBar.cpp
 * @author Tomas Polasek
 * @brief Manu on top of the main window.
 */

#include "MenuBar.h"

namespace mw
{
    MenuBar::type_signal_quit_app MenuBar::signal_quit_app()
    {
        return mSignalQuitApp;
    }

    Glib::RefPtr<MenuBar> MenuBar::create()
    {
        static auto builder = Gtk::Builder::create();
        try {
            builder->add_from_string(ui::menuBar);
        } catch (const Glib::Error &err) {
            std::cerr << "Unable to build WorkArea: \n"
                      << err.what() << std::endl;
        }

        MenuBar *result{nullptr};
        builder->get_widget_derived("menu", result);

        return Glib::RefPtr<MenuBar>(result);
    }

    void MenuBar::setWorkArea(WorkArea *workArea)
    {
        mWorkArea = workArea;
    }

    void MenuBar::reset()
    {
        mWorkArea->loadConfig(mCurrentConfig);
    }

    void MenuBar::defaultSettings()
    {
        mWorkArea->loadConfig(cfg::DEFAULT_CFG);
        mCurrentConfig = cfg::DEFAULT_CFG;
    }

    void MenuBar::cfgLoad()
    {
        Gtk::FileChooserDialog dialog("Load configuration.", Gtk::FILE_CHOOSER_ACTION_OPEN);
        dialog.set_current_folder("../examples");
        auto filter = Gtk::FileFilter::create();
        filter->add_pattern("*.cfg");
        filter->set_name("Configuration file (*.cfg)");
        dialog.set_filter(filter);
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        dialog.set_transient_for(*window);

        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Load", Gtk::RESPONSE_OK);

        int result = dialog.run();
        dialog.hide();

        switch (result)
        {
            case Gtk::RESPONSE_OK:
            {
                try {
                    auto fileName = dialog.get_filename();

                    std::ifstream file(fileName);
                    if (!file)
                    {
                        throw std::runtime_error("Unable to open the file!");
                    }

                    std::string cfgContent;

                    // Reserve the string size upfront.
                    file.seekg(0, std::ios::end);
                    cfgContent.reserve(file.tellg());
                    file.seekg(0, std::ios::beg);

                    // Read the whole file into the string.
                    cfgContent.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

                    mWorkArea->loadConfig(cfgContent);

                    mCurrentConfig = cfgContent;
                } catch (const std::runtime_error &err) {
                    displayError(err.what());
                } catch (...) {
                    displayError("Unknown error occurred!");
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    void MenuBar::cfgSave()
    {
        Gtk::FileChooserDialog dialog("Save configuration.", Gtk::FILE_CHOOSER_ACTION_SAVE);
        dialog.set_current_folder("../examples");
        dialog.set_do_overwrite_confirmation(true);
        auto filter = Gtk::FileFilter::create();
        filter->add_pattern("*.cfg");
        filter->set_name("Configuration file (*.cfg)");
        dialog.set_filter(filter);
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        dialog.set_transient_for(*window);

        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Save", Gtk::RESPONSE_OK);

        int result = dialog.run();
        dialog.hide();

        switch (result)
        {
            case Gtk::RESPONSE_OK:
            {
                try {
                    auto fileName = dialog.get_filename();
                    if (util::getExtension(fileName) != ".cfg")
                    {
                        fileName += ".cfg";
                    }
                    auto configContent = mWorkArea->getConfig();

                    std::ofstream file(fileName);
                    if (!file)
                    {
                        throw std::runtime_error("Unable to open the file!");
                    }
                    file << configContent;
                } catch (const std::runtime_error &err) {
                    displayError(err.what());
                } catch (...) {
                    displayError("Unknown error occurred!");
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    void MenuBar::imgExport()
    {
        Gtk::FileChooserDialog dialog("Export image.", Gtk::FILE_CHOOSER_ACTION_SAVE);
        dialog.set_do_overwrite_confirmation(true);
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        dialog.set_transient_for(*window);

        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Export", Gtk::RESPONSE_OK);

        int result = dialog.run();
        dialog.hide();

        switch (result)
        {
            case Gtk::RESPONSE_OK:
            {
                try {
                    auto fileName = dialog.get_filename();

                    mWorkArea->exportImage(fileName);
                } catch (const std::runtime_error &err) {
                    displayError(err.what());
                } catch (...) {
                    displayError("Unknown error occurred!");
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    void MenuBar::animExport()
    {
        displayError("Warning: Based on selected resolution and supersampling multiplier "
                             "the generation of the animation can take a couple of minutes. "
                             "In the mean time the application may seem unresponsive - this "
                             "is completely normal (or at least it is at the current stage "
                             "of development of this application :) ) . If the export fails "
                             "you should also try lowering the target resolution/supersampling "
                             "multiplier. Generated frame numbers should be written to the "
                             "terminal output of the application (this is a temporary way to "
                             "inform the user).");
        Gtk::FileChooserDialog dialog("Export animation.", Gtk::FILE_CHOOSER_ACTION_SAVE);
        dialog.set_do_overwrite_confirmation(true);
        auto filter = Gtk::FileFilter::create();
        filter->add_pattern("*.gif");
        filter->set_name("Animation (*.gif)");
        dialog.set_filter(filter);
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        dialog.set_transient_for(*window);

        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Export", Gtk::RESPONSE_OK);

        int result = dialog.run();
        dialog.hide();

        switch (result)
        {
            case Gtk::RESPONSE_OK:
            {
                try {
                    auto fileName = dialog.get_filename();

                    mWorkArea->exportAnimation(fileName);
                } catch (const std::runtime_error &err) {
                    displayError(err.what());
                } catch (...) {
                    displayError("Unknown error occurred!");
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    void MenuBar::quit()
    {
        mSignalQuitApp.emit(true);
    }

    void MenuBar::about()
    {
        Gtk::AboutDialog dialog;
        dialog.set_program_name("Quantizer");
        dialog.set_version("Version 1.0");
        dialog.set_comments("Quantizer is an application which can be used for creation of "
                                    "artistic imagery by using quantization of functions.");
        std::vector<Glib::ustring> authors;
        authors.push_back("Thomas Polasek (xpolas34@stud.fit.vutbr.cz)");
        authors.push_back("https://github.com/T0mt0mp/FIT-VIN");
        dialog.set_authors(authors);
        dialog.set_logo_icon_name("dialog-information");
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        dialog.set_transient_for(*window);

        dialog.run();
    }

    void MenuBar::displayError(const std::string &message)
    {
        Gtk::Window *window = dynamic_cast<Gtk::Window*>(get_toplevel());
        Gtk::MessageDialog dialog(*window, message, false,
                                  Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dialog.run();
    }

    MenuBar::MenuBar(BaseObjectType *object,
                     const Glib::RefPtr<Gtk::Builder> &builder) :
        Gtk::MenuBar(object),
        mCurrentConfig(cfg::DEFAULT_CFG)
    {
        builder->get_widget("reset", mReset);
        mReset->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::reset));
        builder->get_widget("default", mDefault);
        mDefault->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::defaultSettings));
        builder->get_widget("cfg_load", mCfgLoad);
        mCfgLoad->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::cfgLoad));
        builder->get_widget("cfg_save", mCfgSave);
        mCfgSave->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::cfgSave));
        builder->get_widget("img_export", mImgExport);
        mImgExport->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::imgExport));
        builder->get_widget("anim_export", mAnimExport);
        mAnimExport->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::animExport));
        builder->get_widget("quit", mQuit);
        mQuit->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::quit));

        builder->get_widget("about", mAbout);
        mAbout->signal_activate().connect(
                sigc::mem_fun(*this, MenuBar::about));
    }

    MenuBar::~MenuBar()
    {

    }
}
