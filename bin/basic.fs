#version 330 core

#DIRECTIVE#
#COORD_DIRECTIVE#
#INPUT_DIRECTIVE#

uniform sampler1D colorS;
uniform float inputTime;
uniform float seed;
uniform vec2 funT;
uniform vec2 cursorPos;
uniform vec2 additional;

in vec2 fragPos;

layout(location = 0) out vec4 color;

/// Magical random generator.
float rand(in vec2 st)
{
    return fract(sin(dot(st.xy, vec2(37.17237, 313.194723))) * (192838.1317 + seed));
}
/// Magical random generator.
float rand(in float s)
{
    return fract(sin(s * 313.194723) * (192838.1317 + seed));
}

/// Simple value-noise.
float noise(in vec2 st)
{
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Corners of the interpolation square.
    float lt = rand(i);
    float rt = rand(i + vec2(1.0f, 0.0f));
    float lb = rand(i + vec2(0.0f, 1.0f));
    float rb = rand(i + vec2(1.0f, 1.0f));

    // Smoothstep the interpolation factor.
    vec2 u = f * f * (3.0 - 2.0 * f);

    // Bilinear interpolation.
    float result = mix(
        mix(lt, rt, u.x),
        mix(lb, rb, u.x),
        u.y
    );

    return result;
}

/// Number of noise layers.
#define NOISE_LAYERS 7

/// Fractional brownian motion.
float fbm(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += noise(st) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Turbulence noise.
float turb(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += abs(2.0f * noise(st) - 1.0f) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Ridge noise.
float ridge(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        float n = amplitude - abs(2.0f * noise(st) - 1.0f) * amplitude;
        n = n * n;
        result += n;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

void main()
{
    // Coordinates.
    float x = 0.0;
    float y = 0.0;
    
#if defined(POLAR)
    {
        float r = length(fragPos) * additional.x;
        float a = cos(atan(fragPos.y, fragPos.x) * additional.y);
        x = r;
        y = a;
    }
#elif defined(CIRCULAR)
    {
        float l = dot(fragPos, fragPos) * additional.y;
        x = additional.x * fragPos.x / l;
        y = additional.x * fragPos.y / l;
    }
#else
    {
        x = fragPos.x;
        y = fragPos.y;
    }
#endif
    // Coordinate vector.
    vec2 p = vec2(x, y);
    // Time.
    float t = inputTime;
    // Mouse cursor position.
	vec2 c = cursorPos;
#if defined(SIMPLE)
    float pst = #SRC#;
    color = vec4(texture(colorS, (pst - funT.y) * funT.x));
#elif defined(ORBIT)
    float orb = 0.0;
    // Start with largest possible number = infinity!
    float pst = 1.0 / 0.0;
    vec2 val = #SRC#;
    #ORB#
    color = vec4(texture(colorS, (pst - funT.y) * funT.x));
#elif defined(ADVANCED)
    float pst = 0.0;
    {
        #SRC#;
    }
    color = vec4(texture(colorS, (pst - funT.y) * funT.x));
#else
    // Default color.
    color = vec4(texture(colorS, 0.0));
#endif
}
