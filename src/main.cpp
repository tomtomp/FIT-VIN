/**
 * @file main.cpp
 * @author Tomas Polasek
 * @brief Main function and application construction.
 */

#include "main.h"

int main(int argc, char *argv[])
{
    try {
        // Initialize ImageMagick library.
        Magick::InitializeMagick(*argv);
        // Start the application.
        auto app = Application::create();
        return app->run(argc, argv);
    } catch (const std::runtime_error &err) {
        std::cerr << "Exception escaped the main loop: \n"
                  << err.what() << std::endl;
    }
}

